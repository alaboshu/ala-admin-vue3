import ala from '@/service/ala'
export default {
  // 表格操作方法
  tableActionMethod(intance, action, rowScope) {
    if (ala.strIsEmpty(action)) {
      intance.$alert('操作按钮配置错误', '操作按钮配置错误', {
        confirmButtonText: '确定'
      })
      return
    }
    if (action.type === 'event') {
      if (ala.strIsEmpty(action.method)) {
        intance.$alert('事件名称不能为空', '事件名称不能为空', {
          confirmButtonText: '确定'
        })
        return
      }
      if (action.method !== 'copy') {
        intance.$emit(action.method, rowScope)
      }
    }
    // links
    if (action.type === 1) {
      if (ala.strIsEmpty(action.url)) {
        intance.$alert('Url链接不能为空', 'Url链接不能为空', {
          confirmButtonText: '确定'
        })
      }

      if (action.url !== undefined && rowScope !== undefined && action.url.indexOf('[[') !== -1) {
        var linkUrl = action.url.replace('[[', 'linkBegin')
        var toUrl = linkUrl.match(/(\S*)linkBegin/)[1]
        linkUrl = linkUrl.replace(']]', 'linkEnd')
        var query = linkUrl.match(/linkBegin(\S*)linkEnd/)[1]
        if (query !== undefined && query !== null) {
          query = this.replaceStr(query)
          var queryValue = rowScope[query]
          toUrl = toUrl + queryValue
        }
        intance.$router.push({
          path: toUrl
        })
      } else {
        var routeQueryEdit = intance.$route.query
        var query = {
          ...routeQueryEdit
        }
        if (rowScope) {
          query.id = rowScope.id
          if (!query.id) {
            query.id = rowScope._id
          }
        }
        intance.$router.push({
          path: action.url,
          query
        })
      }
    }
  },
  // 列表页删除事件
  async columnDeleteMethod(intance, action, rowScope) {
    if (ala.strIsEmpty(action.url)) {
      intance.$alert('删除接口ApiUrl不能为空，请配置ApiUrl', 'ApiUrl不能为空', {
        confirmButtonText: '确定'
      })
      return
    }
    if (ala.strIsEmpty(rowScope)) {
      intance.$alert('选择的行数据不存在', '数据不存在', {
        confirmButtonText: '确定'
      })
      return
    }
    if (!rowScope.id) {
      rowScope.id = rowScope._id
    }
    if (ala.strIsEmpty(rowScope.id)) {
      intance.$alert('行数据Id不存在', 'Id不存在', {
        confirmButtonText: '确定'
      })
      return
    }
    var name = rowScope.name
    if (ala.strIsEmpty(name)) {
      name = '该数据'
    }

    try {
      await intance.$confirm('此操作将删除' + name + ',是否继续?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
      var response = this.delete(intance, action.url, rowScope)
      return response
    } catch (error) {
      intance.$message({
        type: 'info',
        message: '已取消删除'
      })
    }
  },
  delete(intance, apiUrl, rowScope) {
    let parament = {
      id: rowScope.id,
      ...intance.$ala.urlToObject()
    }
    if (parament.type === undefined) {
      if (intance.$route.query.key !== undefined) {
        parament.type = intance.$route.query.key
      }
    }
    if (parament.type === undefined) {
      if (intance.type) {
        parament.type = intance.type
      }
    }
    return new Promise((resolve, reject) => {
      ala.httpDelete(apiUrl, parament).then(response => {
        if (response) {
          intance.$message({
            type: 'success',
            message: '删除成功!'
          })
          resolve(true)
        } else {
          resolve(false)
        }
      })
    })
  },
  // 首字母小写
  replaceStr(str) {
    var strTemp = '' // 新字符串
    for (var i = 0; i < str.length; i++) {
      if (i === 0) {
        strTemp += str[i].toLowerCase() // 第一个
        continue
      }
      if (str[i] === ' ' && i < str.length - 1) { // 空格后
        strTemp += ' '
        strTemp += str[i + 1].toLowerCase()
        i++
        continue
      }
      strTemp += str[i]
    }
    return strTemp
  }
}
