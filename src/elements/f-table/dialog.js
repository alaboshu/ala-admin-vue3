// 弹窗操作，比如审核等
import ala from '@/service/ala'
export default {
  // 保存操作
  async saveForm(jsThis, models) {
    models.userId = ala.urlId()
    if (
      ala.strIsEmpty(jsThis.dialogForm.columnActionView.service.postApi)
    ) {
      var type = jsThis.dialogForm.columnActionType
      if (jsThis.$route.query.id) {
        models.id = jsThis.$route.query.id
      }
      let parameter = {
        type: type,
        userId: ala.urlId(),
        model: JSON.stringify(models)
      }
      var response = await ala.httpPost(
        'Api/auto/save',
        parameter
      )
      ala.toMessage(response)
      jsThis.init()
    } else {
      // 使用专用API接口保存
      var para = {
        userId: ala.urlId(),
        ...models
      }
      response = await ala.httpPost(
        jsThis.dialogForm.columnActionView.service.postApi,
        para
      )
      ala.toMessage(response)
      jsThis.init()
    }
  },
  async columnAction(jsThis, data, scope) {
    let par = {
      type: data.type,
      userId: ala.urlId(),
      id: scope.id
    }
    jsThis.dialogForm.columnActionType = data.type
    var response = await ala.httpGet('api/Auto/Form', par)
    if (response) {
      jsThis.dialogForm.columnActionView = response
      jsThis.dialogForm.showColumnActionView = true
    }
    if (jsThis.$refs.dialogAutoForm !== undefined) {
      jsThis.$refs.dialogAutoForm.$emit(
        'form_change_widget_data',
        jsThis.dialogForm.columnActionView
      )
    }
    jsThis.dialogVisible = true
  }
}
