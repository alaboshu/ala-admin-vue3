// 搜索、标签、分页，相关操作
// 点击标签

import convert from './convert'
import format from './format'
import ala from '@/service/ala'
import api from '@/service/api'
export default {
  beforeInit(jsThis, type, columns) {
    // 数据初始化
    jsThis.dataResult = {
      tableActions: [],
      tabs: [],
      searchOptions: [],
      icon: null,
      name: null,
      result: {
        result: [], // 数据源,
        pageSize: 15,
        pageCount: 1,
        pageIndex: 1,
        recoudSite: 1,
        recordCount: 0
      }
    }
    jsThis.async = false
    jsThis.loading = true
    // 以上为数据初始化
    jsThis.dataSourceConfig = {
      type: jsThis.type,
      columns: jsThis.columns
    }
    if (!jsThis.dataSourceConfig.type) {
      jsThis.dataSourceConfig.type = type
    }
    if (columns) {
      jsThis.dataSourceConfig.columns = columns
    }
    if (!jsThis.dataSourceConfig.type) {
      // 从路由获取参数
      jsThis.dataSourceConfig.type = ala.urlType()
    }
    if (!jsThis.dataSourceConfig.type) {
      if (!jsThis.toConfig) {
        jsThis.$alert('类型type不能为空')
        return
      }
    }
    format.style(jsThis)
    convert.to(jsThis)
  },
  // 获取数据
  async fetchDatas(jsThis) {
    if (jsThis.data) {
      if (!jsThis.dataResult) {
        jsThis.dataResult = {}
      }
      jsThis.dataResult = jsThis.data
      jsThis.loading = false
      return
    }

    jsThis.loading = true
    let {
      fetchHandlersPara
    } = jsThis
    let apiUrl = jsThis.apiUrl

    var parameters = {}

    fetchHandlersPara = {
      ...fetchHandlersPara,
      ...parameters,
      pageIndex: jsThis.dataResult.pageIndex,
      pageSize: jsThis.dataResult.pageSize,
      type: jsThis.dataSourceConfig.type
    }
    if (!jsThis.isDateSelect) {
      fetchHandlersPara = {
        ...fetchHandlersPara,
        ...ala.urlToObject()
      }
    } else {
      fetchHandlersPara = {
        ...fetchHandlersPara,
        isSelect: jsThis.isDateSelect
      }
    }

    // 附加参数
    if (jsThis.appendPara) {
      fetchHandlersPara = {
        ...fetchHandlersPara,
        ...jsThis.appendPara
      }
    }
    if (!fetchHandlersPara.pageIndex) {
      fetchHandlersPara.pageIndex = 1
    }
    if (!fetchHandlersPara.pageSize) {
      fetchHandlersPara.pageSize = 15
    }
    var response = await ala.paasHttpGet(apiUrl, fetchHandlersPara)
    if (response) {
      jsThis.dataResult = response
      if (jsThis.dataSourceConfig.columns) {
        jsThis.dataResult.columns = jsThis.dataSourceConfig.columns
      }
      jsThis.loading = false
      jsThis.async = true
      jsThis.$nextTick(() => {
        if (jsThis.$refs.searchForm) {
          jsThis.$refs.searchForm.init(jsThis.dataResult.searchOptions)
        }
        if (jsThis.$refs.export_table) {
          jsThis.$refs.export_table.init(jsThis.dataResult.searchOptions)
        }
      })
      convert.apply(jsThis)
      convert.handle(jsThis)
    } else {
      jsThis.$notify({
        title: '操作失败',
        type: 'error',
        message: response.message,
        position: 'bottom-right'
      })
    }
    if (!jsThis.dataResult) {
      jsThis.dataResult = []
    }
    // 使用外部配置
    if (jsThis.toConfig) {
      jsThis.dataResult = {
        result: jsThis.dataResult,
        columns: jsThis.toConfig.columns,
        tabs: jsThis.toConfig.tabs,
        tableActions: jsThis.toConfig.tableActions,
        searchOptions: jsThis.toConfig.searchOptions
      }
    }
    jsThis.dataResult.result.result.forEach((element, index) => {
      element.rowIndex = index
    })
    if (ala.isBuild() === false) {
      ala.info('表格参数结果：', fetchHandlersPara, jsThis.dataResult)
    }
    if (ala.filter() === 3 && jsThis.dataResult.tableActions) {
      var tableActions = []
      jsThis.dataResult.tableActions.forEach(element => {
        if (api.roleId(element.id) === true) {
          tableActions.push(element)
        }
      })
      jsThis.dataResult.tableActions = tableActions
    }
  },

  // 搜索
  search(jsThis, paramenter) {
    jsThis.fetchHandlersPara = {
      ...jsThis.fetchHandlersPara,
      ...paramenter
    }
    for (let val in jsThis.fetchHandlersPara) {
      if (jsThis.fetchHandlersPara[val] === '' || jsThis.fetchHandlersPara[val] === 0 || jsThis.fetchHandlersPara[val] === undefined) {
        delete jsThis.fetchHandlersPara[val]
      }
    }
    jsThis.fetchHandler()
  },
  // 标签搜索
  tabClick(jsThis, tab, tabIndex) {
    jsThis.dataResult.pageIndex = 1
    if (tabIndex !== '0') {
      var tabResult = {}
      tabResult[tab.name] = tab.key
      jsThis.fetchHandlersPara = {
        ...jsThis.fetchHandlersPara,
        ...tabResult
      }
    } else {
      jsThis.fetchHandlersPara = {}
    }
    jsThis.fetchHandler()
  }
}
