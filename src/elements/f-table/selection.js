import ala from '@/service/ala'
import action from './action'
export default {
  async batchUpdate(jsThis) {
    var rows = jsThis.$refs.table.selection
    if (rows.length <= 0) {
      jsThis.$ala.alert('请选择需要设置的数据')
    }
  },
  // 批量删除
  async batchDelete(jsThis) {
    var rows = jsThis.$refs.table.selection
    if (rows.length <= 0) {
      jsThis.$ala.alert('请选择需要删除的数据')
      return
    }
    try {
      await jsThis.$confirm('此操作将批量删除选中数据，共' + rows.length + '项,是否继续?', '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning'
      })
      rows.forEach(element => {
        var response = action.delete(jsThis, jsThis.dataResult.batchSetting.deleteApi, element)
        if (response) {
          jsThis.dataResult.result.result.splice(element.rowIndex, 1)
        }
      })
    } catch (error) {
      jsThis.$message({
        type: 'info',
        message: '已取消删除'
      })
    }
    jsThis.$refs.table.clearSelection()
  }
}
