import ala from '@/service/ala'
import api from '@/service/api'
export default {
  // 转换表格
  to(jsThis) {
    if (ala.filter() === 3 || ala.filter() === '3') {
      for (let item of jsThis.dataResult.tableActions) {
        if (item.url.toLowerCase().indexOf('api') < 0) {
          item.url = item.url.replace(/\/User\//g, '/Admin/')
        }
      }
    }
    if (ala.filter() === 2 || ala.filter() === '2') {
      for (let item of jsThis.dataResult.tableActions) {
        if (item.url.toLowerCase().indexOf('api') < 0) {
          item.url = item.url.replace(/\/Admin\//g, '/User/')
        }
      }
    }
  },
  // 数据加载完后，表格渲染
  apply(jsThis) {
    // 处理表格排序问题
    if (jsThis.dataResult.columns) {
      if (jsThis.dataResult.columns[0].label !== '操作') {
        jsThis.dataResult.columns.unshift(
          jsThis.dataResult.columns.splice(
            jsThis.dataResult.columns.length - 1,
            1
          )[0]
        )
      }
    }
  },
  // 处理字典格式的数据
  handle(jsThis) {
    var dateItem = null
    if (jsThis.dataResult.columns) {
      jsThis.dataResult.columns.forEach((element, index) => {
        if (element.style.type === 'dictionary') {
          dateItem = JSON.parse(JSON.stringify(element))
          jsThis.dataResult.columns.splice(index, 1)
        }
        if (element.style.type === 'link') {
          if (!api.role(element.style.parameter)) {
            element.style.type = 'normal' // 验证权限
          }
        }
      })
    }
    // dateItem 不为null 时执行
    if (dateItem && jsThis.dataResult && jsThis.dataResult.result && jsThis.dataResult.result.result) {
      jsThis.dataResult.result.result.forEach((element, index) => {
        // 处理表头的数据
        if (index === 0 && element[dateItem.prop]) {
          element[dateItem.prop].forEach((elementChilde, indexChild) => {
            var obj = {
              style: {
                align: 2,
                isShow: true,
                sort: false
              }
            }
            obj.prop = 'code' + indexChild
            obj.style.type = 'code' + indexChild
            obj.style.width = dateItem.style.width
            obj.label = elementChilde.name
            jsThis.dataResult.columns.push(obj)
          })
        }
        // 处理表格内容
        if (element[dateItem.prop]) {
          element[dateItem.prop].forEach((elementChild, childIndex) => {
            element['code' + childIndex] = elementChild.value
          })
        }
      })
    }
  }
}
