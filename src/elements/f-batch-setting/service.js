export default {
  // 视图数据赋值
  getModel(batchItems) {
    var viewModel = {}
    batchItems.forEach(element => {
      switch (element.type) {
        case 'textbox':
          viewModel[element.field] = ''
          break
        case 'switch':
          viewModel[element.field] = false
          break
        case 'numberic':
          viewModel[element.field] = 0
          break
        case 'decimal':
          viewModel[element.field] = 0
          break
        default:
          viewModel[element.field] = ''
      }
    })
    return viewModel
  }
}
