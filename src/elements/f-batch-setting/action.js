export default {
  getValues(rows, selectField, value, actionType, oldStr, newStr) {
    var dic = {}
    rows.forEach(element => {
      var oldValue = element[selectField.field]
      if (actionType === 'equal') {
        dic[element.id] = value
      }
      if (actionType === 'prefix') {
        dic[element.id] = value + oldValue // 前缀
      }
      if (actionType === 'suffix') {
        dic[element.id] = oldValue + value // 后缀
      }
      if (actionType === 'replace') {
        dic[element.id] = oldValue.replace(oldStr, newStr)
      }
      if (actionType === 'add') {
        dic[element.id] = Number(oldValue) + Number(value) // 在原基础上加
      }
      if (actionType === 'reduce') {
        dic[element.id] = Number(oldValue) - Number(value) // 在原基础上减
      }
      if (actionType === 'take') {
        dic[element.id] = Number(oldValue) * Number(value) // 在原基础上乘
      }
      if (actionType === 'divide') {
        dic[element.id] = Number(oldValue) / Number(value) // 在原基础上除
      }
    })
    return dic /*  */
  }
}
