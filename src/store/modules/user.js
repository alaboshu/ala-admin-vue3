import {
  logout,
  getInfo
} from '@/api/login'
import {
  getToken,
  setToken,
  removeToken
} from '@/utils/auth'
import defAva from '@/assets/images/profile.jpg'
import ala from '@/service/ala'
import api from '@/service/api'

const useUserStore = defineStore(
  'user', {
    state: () => ({
      token: getToken(),
      name: '',
      avatar: '',
      roles: [],
      permissions: []
    }),
    actions: {
      // 登录
      login(userInfo) {
        const key = userInfo.key.trim()
        const password = userInfo.password
        var para = {
          key: key,
          password: password
        }
        return new Promise(async (resolve, reject) => {
          await ala.userLogin(para).then(res => {
            setToken(res.token)
            this.token = res.token
            resolve(res)
          }).catch(error => {
            reject(error)
          })
        })
      },
      // 手机号验证码登录
      noteLogin(userInfo) {
        const phonenumber = userInfo.phonenumber.trim()
        const smsCode = userInfo.smsCode
        var para = {
          phonenumber: phonenumber,
          smsCode: smsCode
        }
        return new Promise(async (resolve, reject) => {
          await ala.userCodeLogin(para).then(res => {
            setToken(res.token)
            this.token = res.token
            resolve(res)
          }).catch(error => {
            reject(error)
          })
        })
      },
      // 获取用户信息
      getInfo() {
        console.info('对鞥讨论')
        return new Promise((resolve, reject) => {
          getInfo().then(res => {
            const user = res.data.user
            const avatar = (user.avatar == "" || user.avatar == null) ? defAva : user.avatar;

            if (res.data.roles && res.data.roles.length > 0) { // 验证返回的roles是否是一个非空数组
              this.roles = res.data.roles
              this.permissions = res.data.permissions
            } else {
              this.roles = ['ROLE_DEFAULT']
            }
            this.name = user.userName
            this.avatar = avatar;
            resolve(res)
          }).catch(error => {
            reject(error)
          })
        })
      },
      // 获取手机验证码
      getCode(phonenumber) {
        return new Promise(async (resolve, reject) => {
          if (!ala.checkPhone(phonenumber)) {
            ala.toast('手机号不正确')
            return resolve()
          }
          await api.smsVerifiyCodeGet(phonenumber).then(res => {
            resolve(res)
          }).catch(error => {
            reject(error)
          })
        })
      },
      // 退出系统
      logOut() {
        return new Promise((resolve, reject) => {
          logout(this.token).then(() => {
            this.token = ''
            this.roles = []
            this.permissions = []
            removeToken()
            resolve()
          }).catch(error => {
            reject(error)
          })
        })
      }
    }
  })

export default useUserStore
