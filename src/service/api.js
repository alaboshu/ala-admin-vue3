const api = Object.create(null)
// 自动注册api和apibo-core/src/servie/api下的文件
// 如果文件名，包含__则不注册
var modules =
  import.meta.glob('./api/**/*.js', {
    eager: true
  })
for (const path in modules) {
  Object.assign(api, modules[path])
}

export default {
  ...api
}
