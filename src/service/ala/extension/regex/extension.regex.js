

// 判断身份证号码是否正确
export function checkIdCard(cardId) {
	let reg = /^[1-9]\d{5}(18|19|20|(3\d))\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/
	if (reg.test(cardId)) return true
	else {
		return false
	}
}

// 校验手机号码是否正确
export function checkPhone(phone) {
	let reg = /^[1][3,4,5,6,7,8,9][0-9]{9}$/
	if (reg.test(phone)) return true
	else {
		return false
	}
}