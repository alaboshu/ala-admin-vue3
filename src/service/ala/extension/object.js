import ala from '@/service/ala'
/*
 * 对象转换成驼峰式命名,包括数字和纯对象两种方式
 */
export function objectToHump (obj) {
  if (obj) {
    if (obj instanceof Array) {
      var newArray = []
      obj.forEach(r => {
        var newObj = toHump(r)
        newArray.push(newObj)
      })
      return newArray
    } else if (obj instanceof Object) {
      var newObj = toHump(obj)
      return newObj
    }
  }
}

function toHump (obj) {
  if (obj) {
    var newObj = {}
    Object.keys(obj).forEach(key => {
      var value = obj[key]
      var newkey = ala.strFirstLower(key)
      newObj[newkey] = value
    })
    return newObj
  }
}
