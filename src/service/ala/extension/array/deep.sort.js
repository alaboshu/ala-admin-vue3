import ala from '@/service/ala'

/*
 * 递归排序：支持多级菜单排序
 * @param array ： 数组
 * @param newIndex: 当前选中的数据索引
 * @param oldIndex: 结束的数据索引
 * @retrun :返回排序以后的数组
 */
export function arrayDeepSort(array, newIndex, oldIndex, children = 'children', fatherId = 'fatherId') {
  // 数组的所有元素
  var deepAllArray = ala.arrayDeepAll(array, children)
  var currentRow = deepAllArray.splice(oldIndex, 1)[0]
  var endRow = deepAllArray.splice(newIndex, 1)[0]
  if (!endRow) {
    endRow = deepAllArray.splice(newIndex - 1, 1)[0]
  }
  if (!currentRow || !endRow) {
    return array
  }
  // 删除现有的行
  var sortArray = ala.arrayDeepDel(array, currentRow.id, children)

  // 插入当前选择的元素
  sortArray = ala.arrayDeepInsert(sortArray, currentRow, endRow, children, fatherId)
  return sortArray
}