/*
* 递归插入：根据Id,递归数组，在指定的id后面插入新的元素,目前最多支持三级拖动
* @param array ： 数组
* @param newRow: 数组新元素
* @param endRow: 插入元素的位置
* @retrun :返回加下以后的数组
*/
export function arrayDeepInsert (array, newRow, endRow, children = 'children', fatherId = 'fatherId') {
  if (!array) {
    array = []
  }
  // 顶级拖动,新加元素拖动到顶级
  if (endRow[fatherId] === 0 || endRow[fatherId] === '000000000000000000000000') {
    var arrayIndex = array.indexOf(endRow)
    newRow[fatherId] = endRow[fatherId]
    newRow.level = 1
    array.splice(arrayIndex, 0, newRow)
    return array
  }


  array.forEach(r => {
    if (r.id === endRow[fatherId]) {
      if (!r[children]) {
        r[children] = []
      }
      var arrayIndex = r[children].indexOf(endRow)
      newRow[fatherId] = endRow[fatherId]
      newRow.level = endRow.level + 1
      r[children].splice(arrayIndex, 0, newRow)
    } else if (r[children]) {
      arrayDeepInsert(r[children], newRow, endRow, children, fatherId)
    }
  })

  return array
}

