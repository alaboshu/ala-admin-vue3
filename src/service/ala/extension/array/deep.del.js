/*
* 递归删除：根据Id,递归删除数组中的元素
* @param array ： 数组
* @param id: 要删除的元素id
* @param children: 递归数组子集
* @retrun :返回删除以后的数组
*/
export function arrayDeepDel (array, id, children = 'children') {
  for (let o in array) {
    var element = array[o]
    if (element.id === id) {
      array.splice(o, 1)
      break
    } else {
      if (element[children]) {
        element[children] = arrayDeepDel(element[children], id, children)
      }
    }
  }
  return array
}
