/*
* 递归加入元素：根据Id,递归数组，在指定的id后面插入新的元素,目前最多支持三级拖动
* @param array ： 数组
* @param data: 数组新元素
* @param endRow: 插入元素的位置
* @retrun :返回加下以后的数组
*/
export function arrayDeepAdd (array, data, id, children = 'children', fatherId = 'fatherId') {
  if (!array) {
    array = []
  }
  data.children = []
  if (!array) {
    array = []
  }
  if (data.fatherId === '000000000000000000000000' || data.fatherId === 0) {
    array.push(data)
  } else {
    array.forEach(r => {
      if (r.id === data.fatherId) {
        if (!r.children) {
          r.children = []
        }
        r.children.push(data)
      } else {
        arrayDeepAdd(r.children, data)
      }
    })
  }
  return array
}

