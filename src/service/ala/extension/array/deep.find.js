import ala from '@/service/ala'
/*
 * 根据Id递归查找元素
 * @param array ： 数组
 * @param children: 递归数组子集
 * @retrun :数组
 */
export function arrayDeepFind(array, id, children = 'children') {
  var deepAllArray = ala.arrayDeepAll(array, children)
  var find = deepAllArray.find(r => r.id === id)
  return find
}