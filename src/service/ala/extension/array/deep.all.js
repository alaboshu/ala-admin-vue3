/*
* 递归获取所有元素,包括子元素
* @param array ： 数组
* @param children: 递归数组子集
* @retrun :数组
*/
export function arrayDeepAll (array, children = 'children') {
  var deepArray = []
  deepArray = addItem(deepArray, array, children)
  return deepArray
}


function addItem (deepArray, array, children = 'children') {
  for (var i = 0; i < array.length; i++) {
    var element = array[i]
    deepArray.push(element)
    var childArray = element[children]
    if (childArray) {
      addItem(deepArray, childArray, children)
    }
  }
  return deepArray
}