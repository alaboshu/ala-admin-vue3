// 打开进度条
export function progressOpen(This, message) {
  var data = {
    name: 'zk-progress',
    message: message,
    visible: true
  }
  This.$bus.emit('autoDialogEvent', data)
}
// 关闭进度条
export function progressClose(This) {
  var data = {
    name: 'zk-progress',
    message: '',
    visible: false
  }
  This.$bus.emit('autoDialogEvent', data)
}