// import store from '@/service/store/index'
import ala from '@/service/ala'
import useUserStore from '@/store/modules/user'

export function vuexSet(name, value) {
  useUserStore()[name] = value
}
export function vuexGet(name) {
  return useUserStore()[name]
}
export function vuexLocalGet(name, encrypt = false) {
  var data = vuexGet(name)
  if (data) {
    return data
  } else {
    data = ala.localGet(name, encrypt)
    vuexSet(name, data)
    return data
  }
}

export function vuexRemove(name) {
  useUserStore()[name] = null
}

export function vuexLocalSet(name, value, encrypt = false) {
  vuexSet(name, value)
  ala.localSet(name, value, encrypt)
}


export function vuexLocalRemove(name) {
  vuexRemove(name)
  ala.localRemove(name)
}
