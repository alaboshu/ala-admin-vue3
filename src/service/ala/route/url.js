import ala from '@/service/ala'
// 获取URL中的参数

export function urlQuery(name) {
  var route = ala.router()
  if (route.query !== undefined) {
    return route.query[name]
  }
}

// 获取URL中的ID
export function urlId() {
  var route = ala.router()
  if (route.query !== undefined && route.query.id !== undefined) {
    return route.query.id
  }
  if (route.query !== undefined && route.query.Id !== undefined) {
    return route.query.Id
  }
  if (route.query !== undefined && route.query.ID !== undefined) {
    return route.query.ID
  }
  return 0
}

// 根据路由获取类型
export function urlType() {
  var route = ala.router()
  if (route.query !== undefined && route.query.Type !== undefined) {
    return route.query.Type
  }
  if (route.query !== undefined && route.query.type !== undefined) {
    return route.query.type
  }
  if (route.query !== undefined && route.query.key !== undefined) {
    return route.query.key
  }
  if (route.query !== undefined && route.query.Key !== undefined) {
    return route.query.Key
  }
  if (route.meta !== undefined && route.meta.type !== undefined) {
    return route.meta.type
  }
}




// 图片地址处理
export function urlImage(src) {
  if (src) {
    if (src.indexOf('://') === -1) {
      src = ala.imgHost + src
      src = src.replace('//wwwroot', '/wwwroot')
    } else {
      src = src.replace('//wwwroot', '/wwwroot')
    }
  }
  return src
}


// 处理路由拼接
// 将URL转换成对象 结果格式：test=22&wewe=qqw
export function urlToParams() {
  var data = ala.router()
  if (!data || !data.query) {
    return
  }
  var tempArr = []
  for (var i in data.query) {
    var key = i
    var value = data.query[i]
    // encodeURIComponent(data.query[i])
    tempArr.push(key + '=' + value)
  }
  var urlParamsStr = tempArr.join('&')
  if (urlParamsStr) {
    urlParamsStr = '?' + urlParamsStr
  }
  if (urlParamsStr) {
    return urlParamsStr
  }
  return ''
}



// 将url参数解析为一个对象
export function urlToObject(route) {
  var route = ala.router()
  var str = route.fullPath
  var index = str.indexOf('?', 0)
  str = str.substring(index + 1, str.length)
  var result = {}
  var temp = str.split('&')
  for (var i = 0; i < temp.length; i++) {
    var temp2 = temp[i].split('=')
    if (
      !(temp2[0].indexOf('/') >= 0 || temp2 === undefined || temp2 === null)
    ) {
      result[temp2[0]] = temp2[1]
    }
  }
  return result
}
