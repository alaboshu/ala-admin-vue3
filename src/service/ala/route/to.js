import ala from '@/service/ala'
// 跳转,并记录Url链接地址到换成
export function to(This, item, isDiy = false) {
  if (!item || !item.id || !item.name || !item.url) {
    return
  }
  var list = ala.vuexLocalGet('admin_browsing_history')
  if (list) {
    var isExist = false
    for (var i = 0; i < list.length; i++) {
      var element = list[i]
      if (element.name === item.name) {
        isExist = true
        break
      }
    }
    if (isExist) {
      list = list.filter(r => r.id !== item.id)
    }
    if (!isExist) {
      list.unshift(item)
      if (list.length > 10) {
        list.pop()
      }
    }
  } else {
    list = []
    list.unshift(item)
  }
  ala.vuexLocalSet('admin_browsing_history', list)
  if (item && item.id && item.name && item.url) {
    ala.vuexLocalSet(ala.themeCurrentMenuCacheKey(), item)
  }
  if (isDiy) {
    This.$bus.emit('diyEditMenuJump', item.url)
  } else {
    ala.push(item.url)
  }
}


// 后台管理提示
export function toMessage(This, message, type, title, isStore = false) {
  if (!message) {
    return
  }
  var messageType = 'success'
  if (type) {
    messageType = type
  }
  var data = {
    name: 'admin-message',
    message: message,
    title: title,
    type: messageType
  }
  if (isStore) {
    data.name = 'admin-store-message'
  }
  This.$bus.emit('autoDialogEvent', data)
}