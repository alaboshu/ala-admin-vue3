import routerIndex from '@/router'
import ala from '@/service/ala'


// 在系统启动时提前加载默认启动数据
// 包括枚举、地址等数据
// 当前访问的路由信息
export function router() {
  return routerIndex.currentRoute._value
}

// 当前页面的完整路径
export function fullPath() {
  var fullPath = location.href
  return fullPath
}
// 实际页面路径
export function path(option) {
  var path = option.fullPath
  if (ala.strIsEmpty(path)) {
    path = '/index'
  }
  if (path === '/') {
    path = '/index'
  }

  if (path.indexOf('admin/diy') > -1) {
    routerIndex.push('/admin/diy') // 跳转到diy
  }
  if (path.indexOf('/admin/index.html#') > -1) {
    // 正式环境处理浏览器刷新
    path = path.replace('/admin/index.html#', '')
    routerIndex.push(path)
  }
  path = path.replace('.html', '')
  path = path.replace('/views', '')
  path = path.replace('views/', '')
  var index = path.indexOf('?')
  if (index > 0) {
    path = path.substr(0, index)
  }
  return path
}


// 使用路由的方式推送链接
export function push(url) {
  routerIndex.push(url)
}
