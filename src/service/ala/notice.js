import {
  ElMessage
} from 'element-plus'

export function alert(message, type = 'error') {
  ElMessage({
    showClose: true,
    message: message,
    type: type,
  })
}


export function dialog(This, name, para, width) {
  This.$bus.emit('autoDialogEvent', name, para, width)
}
