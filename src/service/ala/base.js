import config from '@/service/config'
import ala from '@/service/ala'
import api from '@/service/api'
export let host = import.meta.env.VITE_APP_BASE_API
export let imgHost = config.imgHost ? config.imgHost : config.host

// 判断是生成环境还是开发环境
export function isBuild() {
  if (process.env.NODE_ENV === 'development') {
    return false
  } else {
    return true
  }
}
// 设置标题
export async function setTitle(title) {
  var site = await api.siteAsync()
  var name = ''
  if (title) {
    name = title
  }
  if (site) {
    document.title = `${name}--${site.companyName ? site.companyName : ''}  阿拉博数提供技术支持`
  }
}

/*
 *  返回基本配置列表  
 * 正式环境从服务器上读取
 */
export function baseConfig() {
  if (process.env.NODE_ENV === 'development') {
    return config
  } else {
    var configHost = window.localStorage.getItem('service_host_info') || config
    return configHost
  }
}



// 判断是生成环境还是开发环境
export function clientHost() {
  return config.apiBaseUrl
}

// 权限类型 All=1:无权限，匿名，谁都可以访问   User = 2 会员级别 Admin = 3 管理员级别
//  Store = 4 供应商级别 Offline = 5 线下店铺 City = 6 城市合伙人  营销中心 Market = 101
export function filter() {
  var router = ala.router()
  var filter = 1
  if (router.meta && router.meta.filter) {
    filter = router.meta.filter
  } else {
    var themeType = router.params.themetype
    if (themeType) {
      switch (themeType.toLowerCase()) {
        case 'member':
          filter = 2
          break
        case 'admin':
          filter = 3
          break
        case 'store':
          filter = 11
          break
        case 'merchant':
          filter = 9
          break
        case 'servicecenter':
          filter = 6
          break
        case 'province':
          filter = 101
          break
        case 'city':
          filter = 102
          break
        case 'county':
          filter = 103
          break
        case 'regional':
          filter = 106
          break
        case 'shareholder':
          filter = 20
          break
        default:
          filter = 1
      }
    }
  }
  return filter
}