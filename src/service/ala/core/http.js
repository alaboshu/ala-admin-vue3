import axios from 'axios'
import ala from '@/service/ala'
import axiosRequest from '@/utils/request'


export async function httpGet(apiUrl, data) {
  var request = await axiosRequest({
    url: apiUrl,
    method: 'get',
    timeout: 20000,
    params: data
  })
  var response = result(request, apiUrl)
  return response
}

export async function httpId(apiUrl, id) {
  var response = await httpGet(apiUrl, {
    id: id
  })
  return response
}

export async function httpPost(apiUrl, data) {
  var request = await axiosRequest({
    url: apiUrl,
    method: 'post',
    timeout: 20000,
    data: data
  })
  var response = result(request, apiUrl)
  return response
}
//  Put方法：改
export async function httpPut(apiUrl, data) {
  var request = await axiosRequest({
    url: apiUrl,
    method: 'put',
    timeout: 20000,
    data: data
  })
  var response = result(request, apiUrl)
  return response
}
export async function httpDelete(apiUrl, data) {
  var request = await axiosRequest({
    url: apiUrl,
    method: 'delete',
    timeout: 20000,
    data: data
  })
  var response = result(request, apiUrl)
  return response
}



function result(response, apiUrl) {
  if (response) {
    switch (response.code) {
      case 200:
        return response.data
      case 500:
        ala.error(response.data.msg)
        break
      default:
        ala.error('接口请求错误' + ala.host + apiUrl)
    }
  } else {
    ala.error('接口错误' + apiUrl)
  }
}
