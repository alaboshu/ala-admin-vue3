import ala from '@/service/ala'
import axiosRequest from '@/utils/request.net'


export async function paasHttpGet(apiUrl, data) {
  var request = await axiosRequest({
    url: apiUrl,
    method: 'get',
    timeout: 20000,
    params: data
  })
  var response = result(request, apiUrl)
  return response
}

export async function paasHttpId(apiUrl, id) {
  var response = await paasHttpGet(apiUrl, {
    id: id
  })
  return response
}

export async function paasHttpPost(apiUrl, data) {
  var request = await axiosRequest({
    url: apiUrl,
    method: 'post',
    timeout: 20000,
    data: data
  })
  var response = result(request, apiUrl)
  return response
}
//  Put方法：改
export async function paasHttpPut(apiUrl, data) {
  var request = await axiosRequest({
    url: apiUrl,
    method: 'put',
    timeout: 20000,
    data: data
  })
  var response = result(request, apiUrl)
  return response
}
export async function paasHttpDelete(apiUrl, data) {
  var request = await axiosRequest({
    url: apiUrl,
    method: 'delete',
    timeout: 20000,
    data: data
  })
  var response = result(request, apiUrl)
  return response
}



function result(response, apiUrl) {
  if (response) {
    switch (response.status) {
      case 1:
        return response.result
      case 500:
        ala.error(response.msg)
        break
      default:
        ala.error('接口请求错误' + ala.host + apiUrl)
    }
  } else {
    ala.error('接口错误' + apiUrl)
  }
}
