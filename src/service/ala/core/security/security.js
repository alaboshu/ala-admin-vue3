import {
  baseConfig
} from '@/service/ala/base'
import CryptoJS from 'crypto-js' // 引用AES源码js
var {
  projectId,
  privateKey
} = baseConfig()
let cryptoKey = CryptoJS.MD5(projectId + privateKey + 'www__5ug__com').toString()
const iv = CryptoJS.enc.Utf8.parse(cryptoKey.substring(1, 16)) // 十六位十六进制数作为密钥偏移量

/*
 * 解密方法
 */
export function decrypt(word, securityKey = '') {
  var key = getKey(securityKey)
  let encryptedHexStr = CryptoJS.enc.Hex.parse(word)
  let srcs = CryptoJS.enc.Base64.stringify(encryptedHexStr)
  let decrypt = CryptoJS.AES.decrypt(srcs, key, {
    iv: iv,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7
  })
  let decryptedStr = decrypt.toString(CryptoJS.enc.Utf8)
  return decryptedStr.toString()
}

/*
 * 加密方法
 */
export function encrypt(word, securityKey = '') {
  var key = getKey(securityKey)
  let srcs = CryptoJS.enc.Utf8.parse(word)
  let encrypted = CryptoJS.AES.encrypt(srcs, key, {
    iv: iv,
    mode: CryptoJS.mode.CBC,
    padding: CryptoJS.pad.Pkcs7
  })
  return encrypted.ciphertext.toString().toUpperCase()
}

function getKey(securityKey) {
  if (!securityKey) {
    securityKey = ''
  }
  var encryptKey = securityKey + cryptoKey
  var md5 = CryptoJS.MD5(encryptKey).toString()
  const key = CryptoJS.enc.Utf8.parse(md5) // 32位十六进制数作为密钥
  return key
}
