import ala from '@/service/ala'


/**
 * 安全入口,在登陆页面页面
 * 后台首页母版页引用
 */

export async function securityAdminSign(This) {
    if (ala.filter() !== 3) {
        // 非后台管理不验证
        return
    }
    var route = ala.router()
    if (route.path.toLowerCase().indexOf('/web/') > -1) {
        return
    }
    var sign = ala.localGet('security_sign', true)
    var timestamp = ala.timestamp()
    var token = `${sign}${timestamp}`
    var key = ala.md5(token.toLowerCase())
    var result = await ala.httpGet('Api/Security/Sign', { key: key })
    if (!result) {
        //退出登陆
        This.$ala.userLogout('/admin/login')
    }
}


/**
 * BasicAuth 安全认证
 * 在登陆页面页面
 * 后台首页母版页引用
 */

export async function securityAdminBasicAuth(userName, password) {
    if (ala.filter() !== 3) {
        return
    }
    // 先从缓存中读取
    var key = ala.localGet('security_basicauth', `${password}${userName}`, true)
    if (!key) {
        var timestamp = ala.timestamp()
        var token = `${password}${userName}${timestamp}`
        key = ala.md5(token.toLowerCase())
    }
    var result = await ala.httpGet('Api/Security/BasicAuth', { key: key })
    if (result.status !== 1) {
        ala.localRemove('security_basicauth')
        // 弹窗密码输入框
        return true
    } else {
        ala.localSet('security_basicauth', `${password}${userName}`, true)
        return false
    }
}


