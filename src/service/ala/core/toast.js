import {
  ElMessage
} from 'element-plus'
export function toast(message) {
  ElMessage({
    message: message,
    type: 'success',
    duration: 3000
  })
}

export function success(message) {
  return toast(message)
}
export function warn(message) {
  ElMessage({
    message: message,
    type: 'warning',
    duration: 3000
  })
}

export function error(message) {
  ElMessage({
    message: message,
    type: 'error',
    duration: 4000
  })
}