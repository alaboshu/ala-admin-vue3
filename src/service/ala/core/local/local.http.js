import ala from '@/service/ala'
/*
 * 先从缓存中请求数据，如果没有则从api中请求\
 *  @param name 缓存名称，必须唯一
 *  @param apiUrl api请求接口
 */
export async function localHttp(name, apiUrl) {
  if (!name || !apiUrl) {
    return
  }
  var data = ala.vuexLocalGet(name)
  if (data) {
    return data
  }
  if (!data) {
    var result = await ala.httpGet(apiUrl)
    ala.vuexLocalSet(name, result)
    return result
  }
}