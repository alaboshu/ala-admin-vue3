import ala from '@/service/ala.js'
import api from '@/service/api.js'
const tableKey = 'RemoteLocal' // 远程缓存



/**
 * 设置远程缓存
 * @param {*} name 
 * @param {*} data 
 */
export async function remoteSet (name, data) {
    var para = {
        tableName: tableKey,
        ...data
    }
    var repones = await api.lightAppAdd(para)
}

/**
 * 获取远程缓存
 * @param {获} name 
 * @param {*} encrypt 
 * @returns 
 */
export function remoteGet (name) {

}

/**
 * 获取远程缓存
 * @param {获} name 
 * @param {*} encrypt 
 * @returns 
 */
export function remoteRemove (name) {

}


function getKey (name) {
    if (name.indexOf('alapc__') === 0) {
        return name
    }
    return 'alapc__' + ala.userId() + name
}