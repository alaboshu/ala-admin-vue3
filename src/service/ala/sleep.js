
/**
 * 系统站点时间
 * 单位毫秒，默认2秒
 */

export async function sleep (time = 2000) {
  return new Promise((resolve) => setTimeout(resolve, time))
}