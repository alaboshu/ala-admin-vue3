import {
  baseConfig
} from '@/service/ala/base'

// 当前租户
export function tenant() {
  return 'master'
}

// 是否为租户模式
export function tenantIs() {
  return baseConfig().isTenant
}
