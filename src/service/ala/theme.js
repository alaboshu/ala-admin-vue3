import ala from '@/service/ala'

// 页面信息
export async function themePage(option, type) {
  var path = ala.path(option)
  var pageInfo = await this.getPageInfo(path, type, option)
  if (pageInfo === undefined || pageInfo === null) {
    return
  }
  pageInfo = themeFilterPage(pageInfo, option)
  return pageInfo
}

// 模块信息,para 为附加参数
export async function themeWidget(widget, appendPara) {
  if (widget) {
    return widget.value
  }
}

// 过滤页面信息
export function themeFilterPage(pageInfo, option) {
  var widgets = []
  if (pageInfo.widgets !== null) {
    for (var i = 0; i < pageInfo.widgets.length; i++) {
      var widgetItem = pageInfo.widgets[i]
      widgetItem.route = option
      widgets.push(widgetItem)
    }
    pageInfo.widgets = widgets
  }
  if (!pageInfo.setting) {
    pageInfo.setting = {}
  }
  // if (pageInfo.widgets === null) {
  //   return pageInfo
  // }
  // var filterWidgets
  // if (pageInfo.layout !== 0) {
  //   filterWidgets = pageInfo.widgets.filter(
  //     element => element.layout === pageInfo.layout
  //   )
  // } else {
  //   filterWidgets = pageInfo.widgets
  // }
  pageInfo.widgets = filterWidgets(pageInfo.widgets)
  var setting = pageInfo.setting
  // 登录信息判断
  if (setting && setting.isLogin === true) {
    ala.checkLogin(setting.isLogin)
  }
  return pageInfo
}


// 处理widgets
function filterWidgets(widgets) {
  if (widgets === null || widgets === undefined) {
    return null
  }
  widgets.forEach(element => {
    element.border = null
    if (element.style) {
      if (element.style.border) {
        var styleBorder = JSON.parse(element.style.border)
        element.border = styleBorder
        if (element.border && element.border.name === 'none-border') {
          element.border = null
        }
        if (element.border && element.border.name && element.border.name.length > 0) {
          element.border.show = true
        }
      }
    }
    if (!element.border) {
      element.border = {}
      element.border.show = false
    }

    if (ala.strIsEmpty(element.layout)) {
      element.layout = null
    } else {
      if (element.value) {
        if (typeof element.value !== 'object') {
          element.value = JSON.parse(element.value)
        }
      }
    }
    // 判断element.value 的类型为object时不在系列化
    if (element.value && typeof element.value !== 'object') {
      element.value = JSON.parse(element.value)
    }
  })
  return widgets
}

// 当前访问的页面
export async function getPageInfo(path, type) {
  var allPageInfo = await this.themeAllPage(type)
  var findPageInfo = null
  if (allPageInfo !== undefined && allPageInfo != null) {
    for (var i = 0; i < allPageInfo.pageList.length; i++) {
      var element = allPageInfo.pageList[i]
      if (
        path
        .substring(0, 6)
        .toLowerCase()
        .indexOf('admin') !== -1 &&
        type !== undefined
      ) {
        if (path.toLowerCase() === element.path.toLowerCase()) {
          findPageInfo = element
          break
        }
      } else {
        if (element.path === path) {
          findPageInfo = element
          break
        }
      }
    }
  }
  return findPageInfo
}

// 所有页面记录，并写入缓存 type类型：Front，Admin，Store，City等和后台对应
export async function themeAllPage(type) {
  var allPageCacheKey = ala.themePageCacheKey()
  var allPageInfo = ala.vuexLocalGet(allPageCacheKey)
  var isRequest = true // 是否重新请求
  if (allPageInfo) {
    let timestamp = Math.round(new Date().getTime() / 1000)
    if (allPageInfo.lastUpdate > timestamp) {
      isRequest = false
    }
  }
  if (isRequest) {
    var para = {
      clientType: ala.client(),
      type: ala.themeType()
    }
    var response = await ala.httpGet('Api/Theme/GetAllClientPages', para)
    ala.vuexSet(allPageCacheKey, response)
    ala.localSet(allPageCacheKey, response)
    if (response && response.theme) {
      var setting = response.theme.setting
      if (setting) {
        ala.vuexLocalSet(ala.themeSettingCacheKey(), JSON.parse(setting))
      }
    }
  }
  return allPageInfo
}

/**
 * 模板设置
 */
export function themeSetting() {
  var setting = ala.vuexLocalGet(ala.themeSettingCacheKey())
  if (!setting) {
    setting = {
      themeColor: '#282a3c',
      text1: '#c8c9cc'
    }
  }
  return setting
}

export function theme() {
  ala.vuexLocalGet(ala.themePageCacheKey())
}