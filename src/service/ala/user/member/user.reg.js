import ala from '@/service/ala'

// 注册
export async function userReg(This, model) {
  if (!ala.strIsEmpty(model.password) && model.password.length < 6) {
    This.$notify.warning('密码最短为六位数')
  }
  var response = await ala.httpPost('api/member/reg', model)
  if (response) {
    this.login(model)
  }
}