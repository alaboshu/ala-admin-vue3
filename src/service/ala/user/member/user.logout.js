import ala from '@/service/ala'
import api from '@/service/api'
/*
	* 退出登录
	*/
export async function userLogout(url, clearCache = true) {
	var userKey = ala.userStorageKey()
	ala.localRemove('user_service_token')
	ala.localRemove('shop_order_select_address')
	ala.localSet('user_login_storage_in_vuex', null)
	ala.vuexLocalRemove(userKey)
	ala.vuexLocalRemove(ala.themeRoleCacheKey())
	ala.vuexLocalRemove('user_info')
	ala.vuexLocalRemove('loginUser')
	if (clearCache) {
		api.adminClearCache()
	}
	if (url) {
		ala.push(url)
	}
}