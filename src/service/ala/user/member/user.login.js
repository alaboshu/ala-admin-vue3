import ala from '@/service/ala'
import {
  ElMessage
} from 'element-plus'

/*
 * 登录
 */
export async function userLogin(model, befterTo) {
  if (model.key.length < 3) {
    ElMessage({
      message: '账号最短为 3 个字符',
      type: 'warning',
    })
    return
  }
  if (model.password.length < 6) {
    ElMessage({
      message: '密码最短为六位数',
      type: 'warning',
    })
  }
  var response = await ala.httpPost('employee/login', model)
  if (response) {
    ala.userStorage(response)
    ala.localRemove('wechat_logincount')
    ala.success('恭喜您,登录成功')
  }
  return response
}

/*
 * 手机号验证码登录
 */
export async function userCodeLogin(model) {
  var response = await ala.httpGet('smsLogin', model)
  if (response) {
    ala.success('恭喜您,登录成功')
  }
  return response
}

/*
 * 跳转到登录页面
 */
export function userToLoginPage() {
  ala.erroe('请先登录')
  ala.localSet('browse_historys', window.location.href)
  ala.push('/login')
}


/*
 * 登录后跳转
 */
export async function userLoginAfterTo(befterTo) {
  if (befterTo !== undefined) {
    ala.push(befterTo)
    return
  }
  if (ala.userIsLogin()) {
    // 跳转到上一级页面
    var previousPage = ala.localGet('browse_historys')
    if (ala.strIsEmpty(previousPage)) {
      var themeRoute = await ala.themeRoute()
      ala.push(themeRoute.index)
    } else {
      ala.localRemove('browse_historys')
      ala.push(previousPage)
    }
  }
}
