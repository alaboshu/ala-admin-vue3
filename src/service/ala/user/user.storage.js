// 移动PC共用文件
import ala from '@/service/ala'
import {
  baseConfig
} from '@/service/ala/base'
var {
  projectId,
  privateKey
} = baseConfig()


/*
 * 将用户信息,加密保存到缓存中，和vuex中
 */
export function userStorage(userOutput) {
  if (!userOutput) {
    ala.toastWarn('用户登录失败')
    return
  }
  // 保存服务器上返回的token
  ala.localSet('user_service_token', userOutput.token, true)

  var userStorageKey = ala.userStorageKey()
  if (!userStorageKey) {
    ala.toastWarn('用户缓存Key获取失败')
    return null
  }
  ala.localRemove('user_login_storage_in_vuex')

  // 把用户信息缓存到vuex中
  ala.localSet('user_login_storage_in_vuex', userOutput, true)
  // 加密保存到本地
  ala.localSet(userStorageKey, userOutput, true)
}


/*
 *  本地用户缓存信息key,反正被他人负责恶意修改，每个用户不一样
 */
export function userStorageKey() {
  var userServiceToken = ala.localGet('user_service_token', true)
  if (!userServiceToken) {
    return null
  }
  var tokenKey = userServiceToken + userServiceToken.substring(3, 10) + projectId.substring(1, 4)
  var userKey = ala.md5(tokenKey)
  userKey = 'user_' + userKey.toString().substring(4, 24)
  return userKey
}
