import ala from '@/service/ala'
// 检查是否需要登录，如果需要登录则跳转到登录页面，登录成功以后，返回到上一级页面
export function userCheckLogin (option) {
  if (!userIsLogin()) {
    if (option) {
      var usercode = option.usercode
      if (!ala.strIsEmpty(usercode)) {
        // url 包含推荐码时跳转
        ala.to('/user/reg')
      }
    }
  }
  return true
}

// 是否登录
export function userIsLogin () {
  var user = ala.user()
  if (!user) {
    return false
  } else {
    return true
  }
}