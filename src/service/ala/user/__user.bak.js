import ala from '@/service/ala'
// import crypto from '@/service/utils/crypto'
var crypto = {}

// 当前登录用户
// 登陆用户
export function user() {
  return userInfo()
}

// 是否登录
export function userIsLogin() {
  var loginUser = user()
  if (!loginUser) {
    return false
  } else {
    return true
  }
}

function userKey() {
  return crypto.userKey()
}
// 用户Id
export function userId() {
  var loginUser = user()
  if (!loginUser) {
    return 0
  }
  return loginUser.id
}


// 用户名
export function userName() {
  var loginUser = user()
  if (!loginUser) {
    return null
  }
  return loginUser.username
}



// 检查是否需要登录，如果需要登录则跳转到登录页面，登录成功以后，返回到上一级页面
export function checkLogin(option) {
  if (!userIsLogin) {
    if (option.indexOf('user') !== -1) {
      ala.push('/login')
    }
    if (option.indexOf('admin') !== -1) {
      ala.push('/admin/login')
    }
    ala.localSet('browse_historys', window.location.href)
  }
}

// 将用户信息写入缓存
function setUser(user) {
  if (ala.strIsEmpty(user)) {
    return null
  }
  if (ala.strIsEmpty(user.token)) {
    return null
  }
  if (user.token < 12) {
    return null
  }
  var userToken = user.token
  ala.localSet('user_token', user.token)
  ala.vuexSet('loginUser', user)
  var userText = crypto.encrypt(JSON.stringify(user), userToken)
  ala.localSet(userKey(), userText)
}



function userInfo() {
  var user = ala.vuexLocalGet('loginUser')
  if (user) {
    return user
  } else {
    var user = ala.localGet(userKey())
    if (user) {
      var loginUser = JSON.parse(crypto.decrypt(user, ala.localGet('user_token')))
      ala.vuexSet('loginUser', loginUser)
    } else {
      return
    }
  }
  if (user) {
    // 对加密数据进行base64处理,
    // 将数据先base64还原，再转为utf8数据,再解密数据
    return JSON.parse(crypto.decrypt(crypto.utf8(crypto.base64(user)), ala.localGet('user_token')))
  }
  if (!user) {
    return null
  }
  return user
}
