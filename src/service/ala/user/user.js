// 移动PC共用文件
import ala from '@/service/ala'


/*
* 用户基本信息
* 只包括id,userName,name,gradeId等字段
* 和服务器User表对应
*/
export function user () {
  var userOutput = ala.userOutput()
  if (userOutput) {
    return userOutput.user
  }
}


/*
* 用户Id,可用于快速绑定
*/
export function userId () {
  var loginUser = user()
  if (!loginUser) {
    return 0
  }
  return loginUser.id
}

/*
* 用户名,可用于快速绑定
*/
export function userName () {
  var loginUser = user()
  if (!loginUser) {
    return ''
  }
  return loginUser.user.userName
}

export function urlImage (src) {
  if (src) {
    if (src.indexOf('://') === -1) {
      src = ala.imgHost + src
      src = src.replace('//wwwroot', '/wwwroot')
    } else {
      src = src.replace('//wwwroot', '/wwwroot')
    }
  }
  return src
}
