// 移动PC共用文件
import ala from '@/service/ala'
/*
 * 服务器登录是用户输出
 * 包括user、userDetail、userToken等详细信息
 */
export function userOutput() {
  // 从vuex中获取
  var user = ala.localGet('user_login_storage_in_vuex')
  if (user) {
    return user
  }
  // 从本地加密缓存中获取
  var tokenUserKey = ala.userStorageKey()
  var user = ala.localGet(tokenUserKey, true)
  if (user) {
    ala.localSet('user_login_storage_in_vuex', user)
    return user
  }
  return user
}
