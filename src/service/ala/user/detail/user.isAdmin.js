import ala from '@/service/ala'
// 是否是管理员
export async function userIsAdmin() {
  var userOutput = ala.userOutput()
  if (!userOutput) {
    return false
  }
  // 未定义，从服务器上请求
  if (userOutput.isAdmin === undefined) {
    var result = ala.httpPost('Api/Employee/IsAdmin', userOutput)
    userOutput.isAdmin = result
    ala.userStorage(userOutput)
    return userOutput.isAdmin
  }
}


/**
 *  判断用户是否为当前用户类型
 *  比如判断用户是否为管理员
 * 是否为供应商等
 */
export async function userIsUserType() {
  var userOutput = ala.userOutput()
  if (!userOutput) {
    return false
  }
  var themeRoute = await ala.themeRoute()
  if (!themeRoute) {
    return false
  }
  var roleOutput = ala.vuexLocalGet(ala.themeRoleCacheKey(), true)
  if (!roleOutput) {
    return false
  }
  if (roleOutput.filterType === themeRoute.value) {
    return true
  }
  return false
}