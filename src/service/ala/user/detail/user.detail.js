import ala from '@/service/ala'
/*
* 用户详细信息
*/
export function userDetail () {
  var userOutput = ala.userOutput()
  if (userOutput) {
    var detail = userOutput.detail
    return detail
  }
}


/*
* 用户等级信息
*/
export function userGrade () {
  var userOutput = ala.userOutput()
  if (userOutput) {
    return userOutput.grade
  }
}



/*
* 用户头像
*/
export function userAvator () {
  var userOutput = ala.userOutput()
  var avator = userOutput.detail.avator
  if (!avator) {
    avator = '/wwwroot/static/images/avator/man_64.png'
  }
  avator = ala.urlImage(avator)
  return avator
}

