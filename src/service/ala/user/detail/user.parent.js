import ala from '@/service/ala'
/*
* 用户推荐人信息
*/
export async function userParent () {
  var user = ala.user()
  var para = {
    parentId: user.parentId
  }
  var result = await ala.get('user', para)
  return result
}