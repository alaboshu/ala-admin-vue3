
import ala from '@/service/ala'



/*
* 用户Token信息，包含：
* ExpireTime:过期时间、IsNeedSetPayPassword：是否需要修改支付密码 Token：加密字符串
* 和服务器User表对应
*/
export function userToken () {
  var userOutput = ala.userOutput()
  if (userOutput) {
    return userOutput.token
  }
}



/*
*  登录过期时间
*/
export function userTokenExpireTime () {
  var token = userToken()
  if (token) {
    return token.expireTime
  }
  return 0
}

