import ala from '@/service/ala'

/**
 * 权限缓存key
 */
export function themeRoleCacheKey() {
  var key = 'roleOutput__' + ala.themeType()
  return key
}

/**
 * 模板缓存
 */

export function themePageCacheKey() {
  var key = 'allPageInfo__' + ala.themeType()
  return key
}

/**
 * 模板设置缓存
 */

export function themeSettingCacheKey() {
  var key = 'themeSetting__' + ala.themeType()
  return key
}

/**
 * 当前点击的菜单缓存
 */

export function themeCurrentMenuCacheKey() {
  var key = 'current_menu__' + ala.themeType()
  return key
}

/**
 * 模板设置缓存
 */

export function themeSingleDataCacheKey() {
  var key = 'single_data_reports__' + ala.themeType()
  return key
}