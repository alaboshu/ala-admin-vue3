import ala from '@/service/ala'
// 终端类型
export function client() {
  return 'PcWeb'
}


// 客户端服务器地址，上传图片地址
export function clientUploadApi() {
  var apiUrl = ala.imgHost + 'Api/StorageFile/upload'
  return apiUrl
}