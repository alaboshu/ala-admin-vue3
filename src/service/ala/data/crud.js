import ala from '@/service/ala.js'

/*
 * 添加接口
 * type:实体类。比如User，Product，Order和后台数据库对应
 */
export async function add (type, para) {
  if (!type) {
    ala.error('类型不能为空')
  }
  var apiUrl = `Api/${type}/QueryAdd`
  var result = await ala.httpPost(apiUrl, para)
  return result
}


/*
 * 编辑
 * type:实体类。比如User，Product，Order和后台数据库对应
 */
export async function edit (type, para) {
  if (!type) {
    ala.error('类型不能为空')
  }
  var apiUrl = `Api/${type}/QueryUpdate`
  var result = await ala.httpPost(apiUrl, para)
  return result
}

/*
 * 编辑或添加
 * type:实体类。比如User，Product，Order和后台数据库对应
 */
export async function save (type, para) {
  if (!type) {
    ala.error('类型不能为空')
  }
  var apiUrl = `Api/${type}/QuerySave`
  var result = await ala.httpPost(apiUrl, para)
  return result
}

/*
 * 编辑或添加
 * type:实体类。比如User，Product，Order和后台数据库对应
 */
export async function del (type, para) {
  if (!type) {
    ala.error('类型不能为空')
  }
  var apiUrl = `Api/${type}/QueryDelete`
  var result = await ala.httpDelete(apiUrl, para)
  return result
}