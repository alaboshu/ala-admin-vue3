import ala from '@/service/ala'

/*
 * 根据实体类型和Id获取实体对象，获取视图永远不会为空
 * type:实体类。比如User，Product，Order和后台数据库对应
 */
export async function viewById (type, id) {
  if (!type) {
    ala.error('类型不能为空')
  }
  var apiUrl = `Api/${type}/QueryById`
  var data = {
    id: id
  }
  var result = await ala.httpGet(apiUrl, data)
  return result
}