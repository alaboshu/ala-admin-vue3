import ala from '@/service/ala'

/* 实体list查询
 * @param {*} type：实体类型
 * @param {*} para ：查询参数
 */
export async function list (type, para) {
  if (!type) {
    ala.error('类型不能为空')
  }
  var apiUrl = `Api/${type}/QueryList`
  var result = await ala.httpGet(apiUrl, para)
  return result
}
