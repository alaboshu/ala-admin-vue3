import ala from '@/service/ala.js'

/*
 * 根据实体类型和参数获取实体对象
 * type:实体类。比如User，Product，Order和后台数据库对应
 */
export async function get (type, para) {
  if (!type) {
    ala.error('类型不能为空')
  }
  var apiUrl = `Api/${type}/QuerySingle`
  var result = await ala.httpGet(apiUrl, para)
  return result
}


/*
 * 根据实体类型和Id获取实体对象，数据不存在时返回为空
 */
export async function getById (type, id) {
  if (!type) {
    ala.error('类型不能为空')
  }
  var para = {
    id: id
  }
  var apiUrl = `Api/${type}/QueryById`
  var result = await ala.httpGet(apiUrl, para)
  return result
}

