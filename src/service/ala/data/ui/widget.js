import ala from '@/service/ala'

export async function widget (type, para) {
    if (!type) {
        ala.error('请传入type类型')
    }
    if (!para) {
        para = {}
    }
    para.key = type
    var result = await ala.httpGet('api/widget/Get', para)
    return result
}

export async function widgetPost (type, para) {
    if (!type) {
        ala.error('请传入type类型')
    }
    var paraPost = {
        type: type,
        userId: ala.urlId(),
        ...ala.urlToObject()
    }
    paraPost.model = JSON.stringify(para)
    var result = await ala.httpPost('api/widget/Post', paraPost)
    ala.info('widget请求信息', paraPost, result)
    return result
}