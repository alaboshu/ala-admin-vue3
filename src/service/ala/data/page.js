import ala from '@/service/ala'
/*
 * 根据实体类型和查询参数获取分页信息
 * type:实体类。比如User，Product，Order和后台数据库对应
 * para查询参数{
 *  userId:1,
 *  name:'%c名称'
 * }
 * pageIndex:当前页码，单独传入进来
 */
export async function page (type, pageIndex, para) {
  if (!type) {
    ala.error('类型不能为空')
  }
  var apiUrl = `Api/${type}/QueryPageList`
  if (!pageIndex) {
    pageIndex = 1
  }
  var data = {
    pageIndex: pageIndex
  }
  if (para) {
    data = {
      ...data,
      ...para
    }
  }
  var result = ala.httpGet(apiUrl, data)
  return result
}


