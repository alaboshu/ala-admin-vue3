import ala from '@/service/ala'
// 保存单条记录
export async function relationSave(relation) {
  if (relation.isEnable === true) {
    relation.status = 1
  } else {
    relation.status = 2
  }
  var response = await ala.httpPost('relationAdmin/batchSave', relation)
  return response
}


// 排序
export async function relationSort(array) {
  var saveArray = ala.arrayDeepAll(array)
  var response = await ala.httpPost('relationAdmin/batchSave', saveArray)
  return response
}
