const ala = Object.create(null)
// 自动注册ala和alabo-core/src/servie/ala下的文件
var modules =
  import.meta.glob('./ala/**/*.js', {
    eager: true
  })
for (const path in modules) {
  Object.assign(ala, modules[path])
}

export default {
  ...ala
}
