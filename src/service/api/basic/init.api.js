import ala from '@/service/ala'
import api from '@/service/api'

/*
 * 初始化系统加载数据
 */

export async function init() {
  await ala.themeAllPage('admin')
  await api.addressAll()
  await api.addressCityIndex()
  await api.activityAttributes()
  // await api.typeAll()
  api.enumAll()
  // await Vue.prototype.$bus.$emit('global_loading_theme')
}