import ala from '@/service/ala.js'

/// 加载所有的地址保存到缓存中去
export async function addressAll() {
	var address = ala.localGet('region_address_list')
	if (address) {
		if (address.province && address.city && address.area) {
			return address
		} else {
			ala.localRemove('region_address_list')
		}
	}
	address = {
		province: '',
		city: '',
		area: ''
	}
	var province = await ala.httpGet('api/Region/GetRegionData?level=2')
	address.province = JSON.parse(province)
	var city = await ala.httpGet('api/Region/GetRegionData?level=3')
	address.city = JSON.parse(city)
	var area = await ala.httpGet('api/Region/GetRegionData?level=4')
	address.area = JSON.parse(area)
	ala.localSet('region_address_list', address)
	return address
}

// 城市索引

export async function addressCityIndex() {
	var result = ala.localGet('region_address_city_index')
	if (!result) {
		result = await ala.httpGet('api/Region/CityIndex')
		if (result) {
			ala.localSet('region_address_city_index', result)
		}
	}
	return result
}

/**
	* 根据城市名称获取 区县列表
	* @param  cityName  城市名称
	*/
export async function addressGetCounty(cityName) {
	var para = {
		cityName: cityName
	}
	var result = await ala.httpGet('api/Region/GetCountyByCity', para)
	return result
}

/*
	* 用户自己的地址
	*/
export async function addressUser() {
	var result = await ala.httpGet('api/useraddress/get')
	if (result.length > 0) {
		var dataIndex = result.findIndex(r => r.isDefault === true)
		result.splice(0, 0, result.splice(dataIndex, 1)[0])
	}
	return result
}

/*
	* 编辑地址
	*/
export async function addressEdit(data) {
	var result = await ala.httpPost('api/useraddress/SaveOrderAddress', data)
	return result
}

/*
	* 删除地址
	*/
export async function addressDelete(id) {
	var result = await ala.httpDelete('api/useraddress/delete?id=' + id)
	return result
}

/*
	* 地址详细信息
	*/
export async function addressFullName(id) {
	var result = await ala.httpGet('Api/Region/GetFullName?areaId=' + id)
	if (result.status === 1) {
		return result.message
	}
}