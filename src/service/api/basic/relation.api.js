import ala from '@/service/ala.js'



/*
 *  根据类型获取所有的分类或标签
 *  实例：type=ProductClassRelation
 */
export async function relationGet (type) {
  var para = {
    type: type
  }
  var response = await ala.httpGet('Api/Relation/GetKeyValues', para)
  return response
}

/*
 *  根据类型获取所有的父级分类或标签
 *  实例：type=ProductClassRelation
 */
export function relationFather (type) { }

/*
 *  根据类型获取所有的分类或标签
 *  实例：type=ProductClassRelation
 */
export async function relationGetAll (type) {
  var para = {
    type: type
  }
  var response = await ala.httpGet('Api/Relation/GetKeyValues', para)
  return response
}


/*
 *  获取树形结果股
 *  实例：type=ProductClassRelation
 */
export async function relationTree (type, isDiy = false) {
  var para = {
    type: type
  }
  if (isDiy === true) {
    var response = await ala.diyHttpGet('Api/RelationTree/GetTree', para)
    return response
  } else {
    var response = await ala.httpGet('Api/RelationTree/GetTree', para)
    return response
  }
}


