import ala from '@/service/ala.js'

/*
 *  上传复制到剪切板的图片
 * 上传base64的图片，并且返回路径
 */
export async function uploadCopyImage (data) {
  const { file, blob } = data
  var parameter = {
    type: file.type,
    size: file.size,
    extension: file.type,
    name: file.name,
    miniurl: file.miniurl
  }
  var response = await ala.httpPost('Api/StorageFile/UploadBase64', parameter)
  return response
}
