import ala from '@/service/ala.js'
import {
  baseConfig
} from '@/service/ala/base'

// 获取logo

export async function configLogo() {
  var webSiteConfig = await this.configGet('webSiteConfig')
  if (webSiteConfig) {
    var url = baseConfig().host + webSiteConfig.logo
    url = ala.urlImage(url)
    return url
  }
}

/// 获取配置信息
export async function configGet(name) {
  var para = {
    parameter: name
  }
  var find = null
  var allConfigs = await configAll() // 先从缓冲中读取
  if (allConfigs) {
    find = allConfigs.find(r => r.type.toLowerCase().indexOf(name.toLowerCase()) !== -1)
    if (find) {
      return find.value
    }
  }
  if (!find) {
    var response = await ala.httpGet('api/AutoConfig/GetAutoConfig', para)
    return response
  }
}

/**
 *  根据ID和类型获取设置
 * @param {string} name 
 * @param {string} id 
 */

export async function configById(name, id) {
  var list = await configList(name)
  var find = list.filter(r => r.id === id)
  if (find) {
    return find[0]
  }
}

/// 获取多条数据
export async function configList(name) {
  var para = {
    type: name
  }
  var find = null
  var allConfigs = await configAll() // 先从缓冲中读取
  if (allConfigs) {
    find = allConfigs.find(r => r.type.toLowerCase().indexOf(name.toLowerCase()) !== -1)
    if (find) {
      return find.value
    }
  }
  if (!find) {
    var response = await ala.httpGet('api/AutoConfig/GetAutoConfigList', para)
    return response
  }
}

/// 获取多条数据
export async function configKeyValue(name) {
  var para = {
    type: name
  }
  var response = await ala.httpGet('api/AutoConfig/GetKeyValues', para)
  return response
}

// 所有的AutoConfig配置
export async function configAll() {
  var cacheKey = 'init_all_autoconfigs'
  var data = ala.vuexLocalGet(cacheKey, true)
  if (data) {
    return data
  }
  if (!data) {
    data = await ala.httpGet('api/AutoConfig/QueryList')
    data = data.filter(r => r.type)
    if (data) {
      data.forEach(element => {
        if (element.value) {
          element.value = JSON.parse(element.value)
          element.value = ala.objectToHump(element.value)
        }
      })
      ala.vuexLocalSet(cacheKey, data, true)
      return data
    }
  }
}
