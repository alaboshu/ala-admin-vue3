import ala from '@/service/ala.js'



/**
 *  执行任务，任务埋点，用户点击某些行为时触发,
* @param {*} id  为Guid可以通过后台中获取
 * @param {*} value 本次执行任务的值，比如分享次数，看书时间，签到次数，大多数情况下都是为1
 * @param {*} time 
 * @returns 
 */
export async function task (key, value = 1, time = 500) {
    if (!key) {
        ala.error('任务type不能为空')
        return
    }
    var para = {
        key: key,
        value: value
    }
    setTimeout(async () => {
        // 任务延时执行,不影响主线程
        await ala.httpPost('Api/TaskJob/Execute', para)
    }, 500)
}


/**
 * 当前用户的任务,构建任务大厅
 * @returns  返回当前用户的任务
 */
export async function taskList () {
    let para = {}
    let response = await ala.httpPost('Api/TaskJob/List', para)
    return response
}

