import ala from '@/service/ala.js'
import api from '@/service/api.js'
/**
 * 后台所有任务
 * @param {*} id 
 * @returns 
 */
export async function taskJobAll(key) {
    var para = {
        key: key
    }
    let response = await ala.httpGet('Api/TaskJob/Modules', para)
    setTimeout(async () => {
        await ala.httpGet('Api/TaskJob/Init')
    }, 1500)
    return response
}


/**
 * 后台任务编辑视图
 * @param {*} id 
 * @returns 
 */
export async function taskJobAdminView(key) {
    if (!key) {
        ala.error('任务类型不能为空')
        return
    }
    var para = {
        key: key
    }
    let response = await ala.httpGet('Api/TaskJob/GetEditView', para)
    if (response && response.jobs) {
        var moneyTypeConfigs = await api.configList('MoneyTypeConfig')
        moneyTypeConfigs = moneyTypeConfigs.filter(r => r.status === 1)
        response.jobs.forEach(element => {
            // 处理是否开启字段
            element.title = '开启' + api.enumText('TaskJobExecuteType', element.type)
            // 根据货币类型处理奖励设置
            var rewards = element.rewards
            element.rewards = []
            moneyTypeConfigs.forEach(money => {
                var itemReward = {
                    moneyTypeId: money.id,
                    moneyName: money.name,
                    amount: 0
                }
                var rewardFind = rewards.find(r => r.moneyTypeId === money.id)
                if (rewardFind) {
                    itemReward.amount = rewardFind.amount
                }
                element.rewards.push(itemReward)
            })
        })
    }
    return response
}


/**
 * 后台任务保存
 * @param {*} para 
 * @returns 
 */
export async function taskJobAdminSave(para) {
    if (!para.beginTime) {
        ala.error('开始时间必须填写')
        return
    }
    if (!para.endTime) {
        ala.error('结束时间必须填写')
        return
    }
    if (!para.jobs) {
        ala.error('任务未设置')
        return
    }
    let response = await ala.httpPost('Api/TaskJob/Save', para)
    return response
}

