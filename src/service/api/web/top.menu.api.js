import ala from '@/service/ala.js'

/**
 * 头部导航
 * @param {*} type 
 */

export function topMenus() {
  var links = [{
      name: '首页',
      url: '/index'
    },
    {
      name: '产品功能',
      url: '',
      links: [{
          name: '表单引擎',
          url: '/product/form'
        },
        {
          name: '模板制作',
          url: '/product/theme'
        },
        {
          name: '模块设计',
          url: '/product/widget'
        },
        {
          name: '页面引擎',
          url: '/product/page'
        },
        {
          name: '表单设计',
          url: '/product/Form'
        },
        {
          name: '表格设计',
          url: '/product/table'
        },
        {
          name: '报表设计',
          url: '/product/report'
        },
        {
          name: '门户设计',
          url: '/product/index'
        },
        {
          name: '酷屏设计',
          url: '/product/screen'
        },
        {
          name: '数据结构',
          url: '/product/entity'
        },
        {
          name: '工作流设计',
          url: '/product/bmp'
        },

        {
          name: '代码生成',
          url: '/product/generation'
        },
        {
          name: '仪表盘设计',
          url: '/product/panel'
        },
        {
          name: '轻应用',
          url: '/product/lightApp'
        },
        {
          name: 'API设计',
          url: '/product/api'
        }
      ]
    },
    {
      name: '解决方案',
      url: '',
      links: [{
          name: '表单引擎',
          url: '/product/form'
        },
        {
          name: '模板制作',
          url: '/product/theme'
        },
        {
          name: '模块设计',
          url: '/product/widget'
        },
        {
          name: '页面引擎',
          url: '/product/page'
        },
        {
          name: '表单设计',
          url: '/product/Form'
        },
        {
          name: '表格设计',
          url: '/product/table'
        },
        {
          name: '报表设计',
          url: '/product/report'
        },
        {
          name: '门户设计',
          url: '/product/index'
        },
        {
          name: '酷屏设计',
          url: '/product/screen'
        },
        {
          name: '数据结构',
          url: '/product/entity'
        },
        {
          name: '工作流设计',
          url: '/product/bmp'
        },

        {
          name: '代码生成',
          url: '/product/generation'
        },
        {
          name: '仪表盘设计',
          url: '/product/panel'
        },
        {
          name: '轻应用',
          url: '/product/lightApp'
        },
        {
          name: 'API设计',
          url: '/product/api'
        }
      ]
    },
    // {
    //     name: '定价',
    //     url: '/price'
    // },
    {
      name: '千城旺店',
      url: '/index',
      links: []
    },
    {
      name: '供应链金融',
      url: '/index',
      links: []
    },
    {
      name: '合作伙伴',
      url: '/about/partner/sale'
    },
    {
      name: '帮助',
      url: '',
      links: [{
          name: '零代码文档',
          url: 'about/wiki/zero/index'
        },
        {
          name: '低代码文档',
          url: 'about/wiki/low/index'
        },
        {
          name: 'Java开发文档',
          url: 'about/wiki/java/index'
        },
        {
          name: '管理端开发文档',
          url: 'about/wiki/admin/index'
        },
        {
          name: '前端APP开发文档',
          url: 'about/wiki/app/index'
        },
        {
          name: '后台操作教程',
          url: 'about/developer/index'
        }, {
          name: 'Api文档',
          traget: 'blank',
          url: 'https://apifox.com/apidoc/shared-ab62a309-2dbd-464b-b5b5-04bad5aa24c2'
        }
      ]
    },
    {
      name: '关于我们',
      url: '',
      links: [{
          name: '关于我们',
          url: '/about/company'
        },
        {
          name: '人才招募',
          url: '/about/design/job'
        },
        {
          name: '私有化部署',
          url: '/about/privatize'
        }
      ]

    }
  ]
  return links
}