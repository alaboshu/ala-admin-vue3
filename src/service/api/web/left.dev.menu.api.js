/**
 * 开发者工作台，左侧菜单
 * @returns 
 */
export async function leftDevMenus() {
    var result = {
        background: '#001529',
        color: '#666666',
        activeBackground: '#ffffff'
    }
    result.links =
        [{
            name: '应用概述',
            url: '/theme',
            icon: 'icon-2039-shangch',
            expand: false
        }, {
            name: '模板中心',
            url: '/theme',
            expand: false,
            icon: 'icon-2039-shangch',
            links: [
                {
                    name: '图片管理',
                    url: '/theme'
                }, {
                    name: '图片管理',
                    url: '/theme'
                },
                {
                    name: '图片管理',
                    url: '/theme'
                }
            ]
        }]

    return result
}