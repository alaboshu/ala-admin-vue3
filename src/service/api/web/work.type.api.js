import ala from '@/service/ala.js'

/**
 * 获取当前的themetype数据
 */
export function themetype() {
  var meta = ala.meta()
  if (meta) {
    return meta.themetype
  }
  // 默认为web方式
  return 'web'
}