import api from '@/service/api.js'

/**
 * 根据类型来获取左侧菜单
 * @param {*} type 
 * @returns 
 */
export async function leftMenus(themetype) {
  var result
  switch (themetype) {
    case 'dev':
      result = await api.leftDevMenus()
      break
    case 'design':
      result = await api.leftDesignMenus()
      break
    case 'solution':
      result = await api.leftSolutionMenus()
      break
    default:
      result = await api.leftWebMenus()
  }
  return result
}

/**
 * 正常未登录时的首页菜单
 * @returns 
 */
export async function leftWebMenus() {
  var result = {
    background: '#ffffff',
    color: '#000000',
    activeBackground: '#f1f2f4'
  }
  result.links = [{
      name: '精选推荐',
      url: '/index',
      icon: 'https://diyapi.5ug.com/wwwroot/uploads/api/935-660-657/2022-09-26/6331591f813898ea7ae81763.png',
      expand: false
    }, {
      name: '模板',
      url: '/theme',
      expand: false,
      icon: 'https://diyapi.5ug.com/wwwroot/uploads/api/935-660-657/2022-09-26/63315922813898ea7ae81764.png',
      links: await api.appIndustryConfig()
    },
    {
      name: '页面',
      url: '/theme',
      icon: 'https://diyapi.5ug.com/wwwroot/uploads/api/935-660-657/2022-09-26/63315922813898ea7ae81765.png',
      expand: false,
      links: await api.appPageConfig()
    },
    {
      name: '模块',
      url: '/theme',
      expand: false,
      icon: 'https://diyapi.5ug.com/wwwroot/uploads/api/935-660-657/2022-09-26/63315922813898ea7ae81766.png',
      links: await api.appWidgetConfig()
    },
    {
      name: '创作设计',
      url: '/design/make',
      icon: 'https://diyapi.5ug.com/wwwroot/uploads/api/935-660-657/2022-09-26/63315915813898ea7ae8175c.png',
      expand: false
    },
    {
      name: '招募设计师',
      url: '/about/design/job',
      icon: 'https://diyapi.5ug.com/wwwroot/uploads/api/935-660-657/2022-09-26/6331591d813898ea7ae81762.png',
      expand: false
    }
  ]
  return result
}
