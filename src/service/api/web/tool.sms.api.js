import ala from '@/service/ala'

/**
 * 短信验证码获取
 * @param {验证码} code  
 */
export async function smsVerifiyCodeGet(mobile) {
  var para = {
    mobile: mobile
  }
  var response = await ala.httpGet('sms/code', para)
  return response
}

/**
 * 短信验证 验证 （只单独认证，手机
 * @param {验证码} code 
 * @param {手机号码} code 
 */
export async function smsVerifiyCodeCheck(mobile, code) {
  var para = {
    mobile: mobile,
    code: code
  }
  var response = await ala.httpGet('api/UserReg/CheckVerifiyCode', para)
  return response
}
