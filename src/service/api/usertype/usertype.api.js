import ala from '@/service/ala.js'

/*
*  当前登录的用户类型
*/
export function userType () {
  var roleOutput = ala.vuexLocalGet(ala.themeRoleCacheKey(), true)
  if (roleOutput && roleOutput.model) {
    var userType = roleOutput.model
    if (userType.userId === ala.userId()) {
      return userType
    }
  }
}
