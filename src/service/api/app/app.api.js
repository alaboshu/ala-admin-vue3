import ala from '@/service/ala'
import api from '@/service/api'
/**
 * 获取所有的小程序
 */
export async function appAll(type, keyword, ids) {
  if (!ids) {
    ids = []
  }
  ids = ids.filter(r => r.length > 5)
  var para = {
    type: type,
    keyword: keyword,
    ids: ids
  }
  var response = await ala.httpPost('Api/App/Modules', para)
  return response
}

/**
 * 获取服务号单条数据
 */
export async function appSingle(id) {
  var para = {
    id: id
  }
  var response = await ala.httpGet('Api/App/GetApp', para)
  if (response) {
    return response
  }
}


/**
 * 服务号初始化
 */
export async function appInit() {
  var response = await ala.httpGet('Api/App/Init')
  return response
}


/**
 * 获取小程序类型菜单
 */
export async function appLeftMenu(ids) {
  if (!ids) {
    ids = []
  }
  var types = api.enumGet('AppType')
  if (!types) {
    await api.enumAll()
    types = api.enumGet('AppType')
  }
  var links = []
  types.keyValue.forEach(r => {
    var item = {
      name: r.value,
      type: r.key,
      url: '/Admin/Mp?Type=' + r.key
    }
    if (!ids.includes(r.key)) {
      links.push(item)
    }
  })
  links = links.filter(r => r.type < 20)
  setTimeout(async () => {
    await api.appInit() // 初始化服务号
  }, 1000)

  return links
}
