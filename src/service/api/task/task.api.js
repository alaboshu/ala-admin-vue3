import ala from '@/service/ala'
import api from '@/service/api'
/**
 * 获取所有的任务
 */
export async function taskAll (keyword) {
  var para = {
    keyword: keyword
  }
  var response = await ala.httpGet('Api/TaskJob/Modules', para)
  return response
}
