import ala from '@/service/ala'
import api from '@/service/api'
import {
  ElMessage
} from 'element-plus'

// 模板同步
export async function adminGetDiyTheme() {
  ala.progressOpen(
    '正在同步模板,预计<span style="color: red;">5分钟</span>时间，请勿离开或刷新页面...'
  )
  var site = await api.siteAsync()
  var response = await ala.diyHttpPost('Api/DiyClient/GetAllSiteTheme', site)
  if (response) {
    adminDeleteCache()
    await api.init()
    ala.progressClose()
    ElMessage({
      message: '恭喜您，同步模板成功,共同步' + response + '模板',
      type: 'success'
    })
  } else {
    ala.progressClose()
  }
}

// 修复数据库
export async function adminUpdateDatabase() {
  ala.progressOpen(
    '正在修复数据,预计<span style="color: red;">5分钟</span>时间，请勿离开或刷新页面...'
  )
  var res = await ala.httpGet('Api/Admin/UpdateDataBase')
  if (res) {
    ala.progressClose()
    ElMessage({
      message: '恭喜您，修复数据成功',
      type: 'success'
    })
  } else {
    ala.progressClose()
  }
}

// 初始化
export async function adminInit() {
  ala.progressOpen(
    '正在进行数据初始,预计<span style="color: red;">1分钟</span>时间，请勿离开或刷新页面...'
  )
  var res = await ala.httpGet('Api/Admin/Init')
  if (res) {
    ala.progressClose()
    Vue.prototype.$message({
      message: '恭喜您，数据初始成功',
      type: 'success'
    })
  } else {
    ala.progressClose()
  }
}

// 清空缓存
export async function adminClearCache(signClear) {
  ala.progressOpen(
    '正在清空系统缓存,预计<span style="color: red;">1分钟</span>时间，请勿离开或刷新页面...'
  )
  var res = await ala.httpGet('Api/Admin/ClearCache')
  if (res) {
    await adminDeleteCache(signClear)
    await api.init()
    ala.progressClose()
    Vue.prototype.$message({
      message: '恭喜您，缓存清空成功',
      type: 'success'
    })
    setTimeout(async () => {
      // 清空缓存的时候，自动更新轻应用
      await ala.httpGet('Api/LightConfig/Init')
      // 清空缓存的时候，自动更新下商品价格
      await ala.httpGet('Api/ShopStore/AutoUpdateSkuPrice')
    }, 200)
  } else {
    ala.progressClose()
  }
}

// 清空管理后台模板，没有界面交互操作
export async function adminDeleteCache(signClear) {
  var routes = await ala.themeRoutes()
  routes.forEach(r => {
    ala.vuexLocalRemove('themeSetting__' + r.type.toLowerCase())
    ala.vuexLocalRemove('roleOutput__' + r.type.toLowerCase())
    ala.vuexLocalRemove('allPageInfo__' + r.type.toLowerCase())
  })
  ala.localClear(false, signClear)
  ala.vuexLocalRemove('all_enums_keyvalues')
}

// 转换菜单
export function adminRoleToMenus(roleOutput) {
  if (!roleOutput.menus) {
    roleOutput.menus = []
  }
  // 处理菜单索引，以及是否显示左侧菜单
  roleOutput.menus.forEach((element, index) => {
    var showChildMenu = false
    var asideWidth = '92px'
    if (element.menus && element.menus.length > 0) {
      showChildMenu = true
      asideWidth = '200px'
    }
    element.oneIndex = index
    element.twoIndex = 0
    element.threeIndex = 0
    element.showChildMenu = showChildMenu
    element.asideWidth = asideWidth
    element.level = 1
    if (element.menus) {
      element.menus.forEach((twoMenu, twoIndex) => {
        twoMenu.oneIndex = index
        twoMenu.twoIndex = twoIndex
        twoMenu.threeIndex = 0
        twoMenu.level = 2
        twoMenu.open = false
        if (twoIndex === 0) {
          twoMenu.open = true
        }
        twoMenu.showChildMenu = showChildMenu
        twoMenu.asideWidth = asideWidth
        if (twoMenu.menus) {
          twoMenu.menus.forEach((threeMenu, threeIndex) => {
            threeMenu.oneIndex = index
            threeMenu.twoIndex = twoIndex
            threeMenu.threeIndex = threeIndex
            threeMenu.showChildMenu = showChildMenu
            threeMenu.asideWidth = asideWidth
            threeMenu.level = 3
          })
        }
      })
    }
  })
  return roleOutput
}

// 处理应用中心的菜单
export function adminAppMenus(type, viewModel, data) {
  /**
   * 只针对应用'admin/app'生效
   * 有几种情况
   * 1. 还没生成缓存时， 从api接口获取数据
   * 2. 有缓存时，从缓存获取数据
   * 3. 点击应用页面的内容，左侧菜单显示
   * */
  if (viewModel && viewModel.menus) {
    viewModel.menus.forEach(element => {
      if (type === 'cache' && element.url === '/Admin/App') {
        element.asideWidth = '92px'
        element.showChildMenu = false
      }
      if (type === 'link' && element.url === '/Admin/App') {
        if (data.menus !== null) {
          element.asideWidth = '200px'
          element.showChildMenu = true
        } else {
          element.asideWidth = '92px'
          element.showChildMenu = false
        }
        if (data !== null) {
          ala.to(data)
        }
      }
    })
  }
}