import ala from '@/service/ala'
import api from '@/service/api'
// 员工登录，验证权限的同时访问菜单
export async function employeeLogin() {
  var userOutput = ala.userOutput()
  var themeRoute = await ala.themeRoute()
  if (!userOutput) {
    ala.push(themeRoute.prefix + 'login')
    return
  }
  var result = ala.vuexLocalGet(ala.themeRoleCacheKey(), true)
  if (result) {
    api.adminAppMenus('cache', result)
    return result
  }
  var para = {
    userOutput: userOutput,
    themeId: ala.themeId(),
    filterType: ala.filter()
  }
  var response = await ala.httpPost('Api/Employee/UserTypeLogin', para)
  if (response) {
    var roleOutput = api.adminRoleToMenus(response)
    // 处理菜单索引，以及是否显示左侧菜单
    api.adminAppMenus('cache', roleOutput)
    ala.vuexLocalSet(ala.themeRoleCacheKey(), roleOutput, true)
    return roleOutput
  } else {
    // ala.push('/401')
    ala.userLogout()
    return null
  }
}