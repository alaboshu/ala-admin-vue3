/**
 * 轻应用内容保存
 * @param {string} table 
 *  @param {string} id 
 * @param {data} data 
 */
import api from '@/service/api'

export async function lightAppSave(para) {
  var result
  if (para.id) {
    result = await api.lightAppUpdate(para)
  } else {
    result = await api.lightAppAdd(para)
  }
  if (result) {
    return result
  }
}