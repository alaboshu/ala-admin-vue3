import ala from '@/service/ala'

/**
 *  ligthApp的type类型
 */
export function lightAppType(option) {
  var table = option.TableName
  if (!table) {
    ala.error('轻应用表名不能为空')
    return
  }
  if (table.indexOf('LightApp') < 0) {
    table = 'LightApp' + table
  }
  return table
}


/**
 *  轻应用
 */
export async function lightAppSearch(keyword) {
  var para = {
    keyword: keyword
  }
  var result = await ala.httpGet('Api/LightConfig/List', para)
  setTimeout(async () => {
    await ala.httpGet('Api/LightConfigService/Init')
  }, 1000)
  return result
}