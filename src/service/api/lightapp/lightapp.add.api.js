import ala from '@/service/ala'
/**
 * 轻应用内容添加
 * @param {string} table 
 * @param {data} data 
 */

export async function lightAppAdd (para) {
  if (!para.tableName) {
    ala.error('请输入表名')
  }
  var result = await ala.httpPost('Api/LightApp/add', para)
  if (result && result.status === 1) {
    return true
  }
}
