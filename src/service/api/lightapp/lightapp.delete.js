import ala from '@/service/ala'
/**
 * 获取应用内容列表
 * @param {string} table 
 * @param {data} data 
 */

export async function lightAppDelete(table, id) {
  if (!table) {
    ala.error('请输入表名')
  }
  var data = {
    id: id,
    tableName: table
  }
  var result = await ala.httpDelete('Api/LightApp/Delete', data)
}