import ala from '@/service/ala'
/**
 * 轻应用内容修改保存
 * @param {string} table 
 *  @param {string} id 
 * @param {data} data 
 */

export async function lightAppUpdate(para) {
  var result = await ala.httpPost('Api/LightApp/Update', para)
  if (result.status === 1) {
    return true
  }
}