import ala from '@/service/ala'
/**
 * 获取应用内容列表
 * @param {string} table 
 * @param {data} data 
 */

export async function lightAppPage (table, para, pageIndex) {
  if (!table) {
    ala.error('请输入表名')
  }
  if (!pageIndex) {
    pageIndex = 1
  }
  var data = {
    pageIndex: pageIndex,
    tableName: table
  }
  if (para) {
    data = {
      ...data,
      ...para
    }
  }
  var result = await ala.httpGet('Api/LightApp/GetPagedList', data)
  if (result) {
    if (!result.result) {
      result.result = []
    }
    result.result.forEach(item => {
      item.id = item._id
    })
    return result
  }
}