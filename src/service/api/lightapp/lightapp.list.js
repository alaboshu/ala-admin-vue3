import ala from '@/service/ala'
/**
 * 获取应用内容列表
 * @param {string} table 
 * @param {data} data 
 */

export async function lightAppList (table, para) {
  if (!table) {
    ala.error('请输入表名')
  }
  var para = {
    tableName: table,
    ...para
  }
  var result = await ala.httpGet('Api/LightApp/GetList', para)
  if (result) {
    result.forEach(item => {
      if (item._id !== undefined) {
        item.id = item._id
      }
    })
    return result
  }
}


/**
 * 根据分类Id获取表名
 * @param {String} table 
 * @param {Number} para 
 */
export async function lightAppListByClassId (table, classId) {
  var para = {
    classId: classId
  }
  var result = await lightAppList(table, para)
  return result
}