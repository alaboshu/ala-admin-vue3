import ala from '@/service/ala'
/**
 * 轻应用内容添加
 * @param {string} table 
 * @param {data} data 
 */

export async function lightAppGet (table, para) {
  if (!table) {
    ala.error('请输入表名')
  }
  var para = {
    tableName: table,
    ...para
  }
  var result = await ala.httpGet('Api/LightApp/getsingle', para)
  if (result) {
    result.id = result._id
    return result
  }
}


/**
 * 根据Id获取内容
 * @param {string} table 
 * @param {data} data 
 */

export async function lightAppGetById (table, id) {
  if (!table) {
    ala.error('请输入表名')
  }
  var para = {
    '_id': id
  }
  var result = await lightAppGet(table, para)
  if (result) {
    result.id = result._id
    return result
  }
}