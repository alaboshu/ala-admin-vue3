import ala from '@/service/ala'
import {
  ElMessageBox
} from 'element-plus'

/*
 * 根据实体类型和查询参数获取分页信息
 * type:实体类。比如User，Product，Order和后台数据库对应
 * para查询参数{
 *  userId:1,
 *  name:'%c名称'
 * }
 * pageIndex:当前页码，单独传入进来
 */
export async function entityPageList(type, pageIndex, para) {
  var apiUrl = `Api/${type}/QueryPageList`
  var data = {
    pageIndex: pageIndex
  }
  if (para) {
    data = {
      ...data,
      ...para
    }
  }
  var result = ala.httpGet(apiUrl, data)
  return result
}

/* 实体list查询
 * @param {*} type：实体类型
 * @param {*} para ：查询参数
 */
export async function entityList(type, para) {
  var apiUrl = `Api/${type}/QueryList`
  var result = ala.httpGet(apiUrl, para)
  return result
}

/*
 * 根据实体类型和Id获取实体对象，对象不存在是为空
 * type:实体类。比如User，Product，Order和后台数据库对应
 */
export async function entityById(type, id) {
  var apiUrl = `Api/${type}/QueryById`
  var data = {
    id: id
  }
  var result = ala.httpGet(apiUrl, data)
  return result
}

/*
 * 根据实体类型和Id获取实体对象，获取视图永远不会为空
 * type:实体类。比如User，Product，Order和后台数据库对应
 */
export async function entityViewById(type, id) {
  var apiUrl = `Api/${type}/QueryById`
  var data = {
    id: id
  }
  var result = ala.httpGet(apiUrl, data)
  return result
}


/*
 * 根据实体类型和对象修改或新增
 * type:实体类。比如User，Product，Order和后台数据库对应
 * data:完整的实体对象
 * return：修改或新增后的实体结果
 */
export async function entitySave(type, data) {
  var apiUrl = `Api/${type}/QuerySave`
  var result = ala.httpPost(apiUrl, data)
  return result
}


/*
 * 删除数据
 * type:实体类。比如User，Product，Order和后台数据库对应
 * data:完整的实体对象
 * return：修改或新增后的实体结果
 */
export async function entityDelete(type, id, title) {
  if (!title) {
    title = ''
  }
  var para = {
    id: id
  }
  ElMessageBox.confirm(
      `是否删除${title}?`,
      '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning',
      }
    )
    .then(async () => {
      var response = await ala.httpDelete(`api/${type}/queryDelete`, para)
      if (response) {
        ala.success(`${title}删除成功`)
      }
      return response
    })
    .catch(() => {

    })
  return true
}