import ala from '@/service/ala'
// 随机获取ObjectId
export async function objectId() {
  var response = await ala.httpGet('api/common/GetObjectId')
  if (response) {
    return response
  }
}