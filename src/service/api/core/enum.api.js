import ala from '@/service/ala'
// 获取枚举中的文字

export function enumText(enumType, value) {
  if (!enumType) {
    return value
  }
  var enumFind = enumGet(enumType)
  var itemValue = enumFind.keyValue.find(item => item.key === value)
  if (itemValue) {
    return itemValue.value
  }
  return value
}


export function enumGet(enumType) {
  if (!enumType) {
    return
  }
  var allEnums = enumAll()
  if (!allEnums) {
    return
  }
  var find = allEnums.find(
    item => item.name.toLowerCase() === enumType.toLowerCase()
  )
  if (find) {
    return find
  }
}


// 获取枚举中的文字
export function enumValue(enumType, value) {
  if (!enumType) {
    return value
  }
  var enumFind = enumGet(enumType)
  if (enumFind && enumFind.keyValue) {
    var itemValue = enumFind.keyValue.find(item => item.key === value)
    return itemValue
  }
}


// 系统所有的枚举
export function enumAll() {
  var cacheKey = 'init_all_enums_keyvalues'
  var allEnums = ala.vuexLocalGet(cacheKey, true)
  if (!allEnums) {
    ala.paasHttpGet('api/type/AllEnums').then(response => {
      if (response) {
        ala.vuexLocalSet(cacheKey, response, true)
      }
    })
  }
  allEnums = ala.vuexLocalGet(cacheKey, true)
  return allEnums
}
