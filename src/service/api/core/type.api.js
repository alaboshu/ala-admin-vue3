import ala from '@/service/ala'
// 获取类型的keyvalues对象 // 优先从Url中获取数据
export async function getKeyValues(type, apiUrl) {
  if (apiUrl) {
    var respone = await ala.httpGet(apiUrl)
    if (respone) {
      return respone
    }
  } else {
    var para = {
      type: type
    }
    respone = await ala.httpGet('api/Type/GetKeyValue', para)
    if (respone) {
      return respone
    }
  }
}