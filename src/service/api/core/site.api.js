import ala from '@/service/ala'

// 基本信息

export async function siteAsync() {
  var site = ala.vuexLocalGet('site_default', true)
  if (!site) {
    setTimeout(async () => {
      if (site) {
        return site.site
      }
      var response = await ala.httpGet('api/Tenant/GetDefault', null, false)
      if (response) {
        site = response
        ala.vuexLocalSet('site_default', site, true)
      }
    }, 100)
    return
  }
  return site.site
}

// 基本信息
export function site() {
  var site = ala.vuexLocalGet('site_default', true)
  if (!site) {
    this.siteAsync().then()
    site = ala.vuexLocalGet('site_default', true)
  }
  if (site) {
    return site.site
  }
}

/**
 * 
 * 
 */
export function siteService() {
  var site = ala.vuexLocalGet('site_default', true)
  if (!site) {
    this.siteAsync().then()
    site = ala.vuexLocalGet('site_default', true)
  }
  if (site) {
    return site.site
  }
}