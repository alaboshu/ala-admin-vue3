import ala from '@/service/ala'

/*
 * 根据类型 获取自动表单
 */
export async function uiAutoForm(type) {
  var para = {
    type: type
  }
  var respone = await ala.httpGet('api/Ui/AutoForm', para)
  return respone
}

/*
 * 根据类型 获取自动表单
 */
export async function uiJsonForm(type) {
  var para = {
    type: type
  }
  var respone = await ala.httpGet('api/Ui/JsonForm', para)
  return respone
}

/*
 * 根据类型 获取默认值
 */
export async function uiDefaultValue(type) {
  var para = {
    type: type
  }
  var respone = await ala.httpGet('api/Ui/DefaultValue', para)
  return respone
}