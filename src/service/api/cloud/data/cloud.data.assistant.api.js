import ala from '@/service/ala'

/**
 * 
 * 智能助手
 */
export async function assistantList () {
  var result = ala.httpGet('api/assistant/List')
  return result
}