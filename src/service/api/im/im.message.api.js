import ala from '@/service/ala.js'
import api from '@/service/api.js'

/**
	* 根据临时会话用户
	* 获取历史消息
	* @param {sring} conversation 
	* @param {string} targetId
	* @param {index} pageIndex 
	* */
export async function imMessageHistory(conversation, pageIndex = 1) {
	if (!conversation) {
		ala.error('会话不能为空')
		return
	}
	var para = {
		pageIndex,
		conversationId: conversation.id
	}
	var messageList
	let response = await ala.httpGet('Api/MessageUser/Page', para)
	if (response) {
		if (pageIndex === 1) {
			messageList = response.result
		} else {
			messageList = [...response.result, ...messageList]
		}
	}
	var result = {
		messageList: messageList,
		pageInfo: {
			pageIndex: response.pageIndex,
			pageCount: response.pageCount
		}
	}
	return result
}




/**
	* 删除聊天记录
	*/
export async function imMessageDelete(id) {
	var para = [
		id
	]
	await ala.httpPost('Api/MessageUser/Delete', para)
}
/**
	* 删除多条聊天记录,传入数组
		  示范： var ids = ['61e754f99036bdf7c55ccd5e', '61e754ff9036bdf7c55ccd62']
	*/
export async function imMessageDeleteList(ids) {
	await ala.httpPost('Api/MessageUser/Delete', ids)
}