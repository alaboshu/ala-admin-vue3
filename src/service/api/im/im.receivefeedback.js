import ala from '@/service/ala.js'

export async function msgReceiveFeedback(userId, messageId) {
    if (userId < 1) {
        return
    }
    if (messageId === undefined || messageId.length < 1) {
        return
    }

    var paras = {
        UserId: userId,
        MessageId: messageId
    }

    await ala.httpGet('Api/Message/DeliveryFeedback', paras)
}