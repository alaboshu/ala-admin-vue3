import ala from '@/service/ala.js'


/**
 * 获取客服会话
 */
export async function imQuickreplayList () {
  var para = {
    filterType: ala.filter()
    // userTypeId: ala.
  }
  var find = await ala.list('QuickReply', para)
  find.forEach((element, index) => {
    if (index === 0) {
      element.check = true
    } else {
      element.check = false
    }
  })
  return find
}