import ala from '@/service/ala.js'
/**
	*  发送消息
	* @param {Number} contentType  // 对应后台ImMessageContentType 包括：文字消息(1),语音消息(2),图片消息(4),图文消息(6),位置消息(7),文件消息(8),小视频消息(9),红包消息(22),通知消息(23)等 
	* @param {Object} content  // 内容，传入对象，要和后台消息对象相互对应
	* @param {Object} conversationId   // 会话Id
	*/
export async function imSend(messageContent, conversationId) {
	if (!messageContent) {
		ala.error('内容不能为空')
		return
	}
	var message = {
		type: messageContent.type,
		content: messageContent.content,
		conversationId: conversationId
	}
	await ala.httpPost('Api/Message/Send', message)
}