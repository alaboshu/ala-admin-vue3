import ala from '@/service/ala.js'

/**
	* im用户
	*/
export function imUser() {
	var loginUser = ala.userOutput()
	if (loginUser) {
		var user = {
			id: loginUser.user.id,
			avator: loginUser.detail.avator,
			nickname: loginUser.user.name ? loginUser.user.name : loginUser.user.userName
		}
	} else {
		user = {
			id: 1,
			avator: '/wwwroot/static/images/avator/man_64.png',
			nickname: '匿名用户'
		}
	}
	return user
}