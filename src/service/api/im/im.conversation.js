import ala from '@/service/ala.js'


/**
  * 获取会话列表
  */
export async function imConversationList(searchInput) {
  var para = {
    pageIndex: searchInput.pageIndex,
    keyword: searchInput.keyword,
    pageSize: 15
  }
  var find = await ala.httpGet('Api/ConversationUser/Page', para)
  return find
}



/**
  * 获取当前聊天窗体的回话
 可根据用户Id，和会话ID创建
  */
export async function imConversation(options) {
  if (!options) {
    return
  }
  var para = {
    targetId: options.targetId,
    id: options.id,
    type: options.type
  }
  if (!options.type) {
    options.type = 1 // 默认私聊
  }
  var find = await ala.httpGet('api/Conversation/GetOrSetConversation', para)
  if (find && find.targetUser) {
    ala.setTitle(find.targetUser.nickName)
  }
  return find
}


/**
  * 删除会话
  */
export async function imConversationDelete(id) {
  await ala.httpId('api/ConversationUser/Delete', id)
}