import ala from '@/service/ala.js'


/**
 * 获取客服会话
 */
export async function imCustomerConversations () {
  var find = await ala.httpGet('api/CustomerService/GetConversations')
  return find
}