import ala from '@/service/ala.js'
import api from '@/service/api.js'

/**
 *  消息模块

 */
export async function imMessageModules(keyword) {
  var para = {
    keyword: keyword
  }
  var find = await ala.httpGet('Api/Message/Modules', para)
  return find
}
