import ala from '@/service/ala'

export async function diyTo(themeId, pageId) {
  ala.progressOpen(
    '正在授权登录中,请稍后...'
  )
  var para = {
    themeId: themeId,
    pageId: pageId
  }
  var res = await ala.httpGet('Api/Theme/GetLoginUrl', para)
  setTimeout(() => {
    ala.progressClose()
    if (res) {
      ala.toast('恭喜您，数据初始成功')
    }
  }, 500)
}