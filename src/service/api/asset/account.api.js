import ala from '@/service/ala'
/**
 * 根据资产类别获取资产账号
 * @param {string} typeId 
 */
export async function accountGet (typeId) {
  var para = {
    moneyConfigId: typeId,
    userId: ala.userId()
  }
  var response = await ala.httpGet('Api/Account/GetAccount', para)
  return response
}
