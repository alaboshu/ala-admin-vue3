import ala from '@/service/ala.js'

/*
*  当前登录用户供应商店铺
*/
export function store () {
  var roleOutput = ala.vuexLocalGet(ala.themeRoleCacheKey(), true)
  if (roleOutput && roleOutput.model) {
    var store = roleOutput.model
    if (store.isPlatform) {
      // 平台店铺
      return store
    }
    if (store.userId === ala.userId()) {
      return store
    }
  }
}
