import ala from '@/service/ala.js'

/*
	*  加载所有的活动
	*/
export async function activityAttributes() {
	var result = await ala.localHttp('init_activityAttributes', 'api/Activity/GetAttributes')
	return result
}