const admin = Object.create(null)
// 自动注册ala和alabo-core/src/servie/ala下的文件
var modules =
  import.meta.glob('./admin/**/*.js', {
    eager: true
  })
for (const path in modules) {
  Object.assign(admin, modules[path])
}

export default {
  ...admin
}
