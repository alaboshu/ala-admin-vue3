import {
  createApp
} from 'vue'

import Cookies from 'js-cookie'

import ElementPlus from 'element-plus'
import locale from 'element-plus/lib/locale/lang/zh-cn' // 中文语言

import '@/assets/styles/index.scss' // global css
import App from './App'
import store from './store'
import router from './router'
import directive from './directive' // directive
import VXETable from 'vxe-table'
import 'vxe-table/lib/style.css'

import mitt from 'mitt'
import ala from '@/service/ala'
import api from '@/service/api'
import admin from '@/service/admin'

// 注册指令
import plugins from './plugins' // plugins
import {
  download
} from '@/utils/request'

// svg图标
import 'virtual:svg-icons-register'
import SvgIcon from '@/components/SvgIcon'
import elementIcons from '@/components/SvgIcon/svgicon'

import './permission' // permission control

import {
  useDict
} from '@/utils/dict'
import {
  getConfigKey,
  updateConfigByKey
} from "@/api/system/config";
import {
  parseTime,
  resetForm,
  addDateRange,
  handleTree,
  selectDictLabel,
  selectDictLabels
} from '@/utils/ruoyi'

// 分页组件
import Pagination from '@/components/Pagination'
// 自定义表格工具组件
import RightToolbar from '@/components/RightToolbar'
// 富文本组件
import Editor from "@/components/Editor"
// 文件上传组件
import FileUpload from "@/components/FileUpload"
// 图片上传组件
import ImageUpload from "@/components/ImageUpload"
// 图片预览组件
import ImagePreview from "@/components/ImagePreview"
// 自定义树选择组件
import TreeSelect from '@/components/TreeSelect'
// 字典标签组件
import DictTag from '@/components/DictTag'

const app = createApp(App)

/** 运行5ug main3 自动导入 */
import adminAutoconfigForm from '@/components/admin-autoconfig-form/index.vue'
import adminAutoconfigList from '@/components/admin-autoconfig-list/index.vue'
import adminRelationClass from '@/components/admin-relation-class/index.vue'
import adminRelationTag from '@/components/admin-relation-tag/index.vue'
import adminDeliveryEdit from '@/components/admin-delivery-edit/index.vue'
import adminCategoryEdit from '@/components/admin-category-edit/index.vue'
import zkAutoForm from '@/components/themes/zk-auto-form/index.vue'
import zkFileManage from '@/components/themes/zk-file-manage/index.vue'
import fAudio from '@/elements/f-audio/index.vue'
import fAutoconfig from '@/elements/f-autoconfig/index.vue'
import fBadge from '@/elements/f-badge/index.vue'
import fBatchSetting from '@/elements/f-batch-setting/index.vue'
import fBorder from '@/elements/f-border/index.vue'
import fButton from '@/elements/f-button/index.vue'
import fCheckbox from '@/elements/f-checkbox/index.vue'
import fCode from '@/elements/f-code/index.vue'
import fColor from '@/elements/f-color/index.vue'
import fDataMultiple from '@/elements/f-data-multiple/index.vue'
import fDataSelect from '@/elements/f-data-select/index.vue'
import fDivider from '@/elements/f-divider/index.vue'
import fEditor from '@/elements/f-editor/index.vue'
import fEnum from '@/elements/f-enum/index.vue'
import fGrade from '@/elements/f-grade/index.vue'
import fIcon from '@/elements/f-icon/index.vue'
import fImage from '@/elements/f-image/index.vue'
import fInput from '@/elements/f-input/index.vue'
import fIntro from '@/elements/f-intro/index.vue'
import fLabel from '@/elements/f-label/index.vue'
import fLine from '@/elements/f-line/index.vue'
import fMap from '@/elements/f-map/index.vue'
import fNumber from '@/elements/f-number/index.vue'
import fRadio from '@/elements/f-radio/index.vue'
import fRegion from '@/elements/f-region/index.vue'
import fSelectImage from '@/elements/f-select-image/index.vue'
import fSelectMultiple from '@/elements/f-select-multiple/index.vue'
import fSelect from '@/elements/f-select/index.vue'
import fSwitch from '@/elements/f-switch/index.vue'
import fTab from '@/elements/f-tab/index.vue'
import fTableButton from '@/elements/f-table-button/index.vue'
import fTable from '@/elements/f-table/index.vue'
import search from '@/elements/f-table/search/index.vue'
import xUserTree from '@/elements/f-table/x-user-tree/index.vue'
import fTimeRange from '@/elements/f-time-range/index.vue'
import fTime from '@/elements/f-time/index.vue'
import fTitle from '@/elements/f-title/index.vue'
import fTree from '@/elements/f-tree/index.vue'
import fUpload from '@/elements/f-upload/index.vue'
import fVerification from '@/elements/f-verification/index.vue'
import fVideo from '@/elements/f-video/index.vue'
app.component('admin-autoconfig-form', adminAutoconfigForm)
app.component('admin-autoconfig-list', adminAutoconfigList)
app.component('admin-relation-class', adminRelationClass)
app.component('admin-relation-tag', adminRelationTag)
app.component('admin-delivery-edit', adminDeliveryEdit)
app.component('admin-category-edit', adminCategoryEdit)
app.component('zk-auto-form', zkAutoForm)
app.component('zk-file-manage', zkFileManage)
app.component('f-audio', fAudio)
app.component('f-autoconfig', fAutoconfig)
app.component('f-badge', fBadge)
app.component('f-batch-setting', fBatchSetting)
app.component('f-border', fBorder)
app.component('f-button', fButton)
app.component('f-checkbox', fCheckbox)
app.component('f-code', fCode)
app.component('f-color', fColor)
app.component('f-data-multiple', fDataMultiple)
app.component('f-data-select', fDataSelect)
app.component('f-divider', fDivider)
app.component('f-editor', fEditor)
app.component('f-enum', fEnum)
app.component('f-grade', fGrade)
app.component('f-icon', fIcon)
app.component('f-image', fImage)
app.component('f-input', fInput)
app.component('f-intro', fIntro)
app.component('f-label', fLabel)
app.component('f-line', fLine)
app.component('f-map', fMap)
app.component('f-number', fNumber)
app.component('f-radio', fRadio)
app.component('f-region', fRegion)
app.component('f-select-image', fSelectImage)
app.component('f-select-multiple', fSelectMultiple)
app.component('f-select', fSelect)
app.component('f-switch', fSwitch)
app.component('f-tab', fTab)
app.component('f-table-button', fTableButton)
app.component('f-table', fTable)
app.component('search', search)
app.component('x-user-tree', xUserTree)
app.component('f-time-range', fTimeRange)
app.component('f-time', fTime)
app.component('f-title', fTitle)
app.component('f-tree', fTree)
app.component('f-upload', fUpload)
app.component('f-verification', fVerification)
app.component('f-video', fVideo)


// 全局组件挂载
app.component('DictTag', DictTag)
app.component('Pagination', Pagination)
app.component('TreeSelect', TreeSelect)
app.component('FileUpload', FileUpload)
app.component('ImageUpload', ImageUpload)
app.component('ImagePreview', ImagePreview)
app.component('RightToolbar', RightToolbar)
app.component('Editor', Editor)

// 全局方法挂载
app.config.globalProperties.useDict = useDict
app.config.globalProperties.getConfigKey = getConfigKey
app.config.globalProperties.updateConfigByKey = updateConfigByKey
app.config.globalProperties.download = download
app.config.globalProperties.parseTime = parseTime
app.config.globalProperties.resetForm = resetForm
app.config.globalProperties.handleTree = handleTree
app.config.globalProperties.addDateRange = addDateRange
app.config.globalProperties.selectDictLabel = selectDictLabel
app.config.globalProperties.selectDictLabels = selectDictLabels
app.config.globalProperties.$bus = mitt()
app.config.globalProperties.$ala = ala
app.config.globalProperties.$api = api
app.config.globalProperties.$admin = admin

app.use(router)
app.use(store)
app.use(plugins)
app.use(elementIcons)
app.use(VXETable)
app.component('svg-icon', SvgIcon)

directive(app)

// 使用element-plus 并且设置全局的大小
app.use(ElementPlus, {
  locale: locale,
  // 支持 large、default、small
  size: Cookies.get('size') || 'default'
})

// 修改 el-dialog 默认点击遮照为不关闭
app._context.components.ElDialog.props.closeOnClickModal.default = false

app.mount('#app')
