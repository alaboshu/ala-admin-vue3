import ala from '@/service/ala'
export default {
  handleDelete (jsThis, index, val) {
    if (jsThis.displayPropertys.length === 1) {
      jsThis.$alert('商品属性至少一个，最多三个')
      return
    }
    jsThis.$confirm('是否删除？', {
      confirmButtonText: '确定',
      cancelButtonText: '取消'
    }).then(() => {
      jsThis.displayPropertys.splice(index, 1)
    }).catch(() => {})
  },
  async commodityEdit (jsThis) {
    jsThis.tableList = {
      name: '',
      controlType:'',
      propertyValues: [{
          id: '00000000-0000-0000-0000-000000000000',
          name: ''
        }, {
          id: '00000000-0000-0000-0000-000000000000',
          name: ''
        },
        {
          id: '00000000-0000-0000-0000-000000000000',
          name: ''
        }
      ]
    }
  },
  save(jsThis) {
    if (jsThis.tableList.name === '') {
      jsThis.$alert('名称不能为空')
      return
    }
    for (let i of jsThis.tableList.propertyValues) {
      if (i.valueName === '') {
        jsThis.$alert('属性值不能为空')
        return
      }
    }
    jsThis.dialogVisible = false
    var list = []
    for (let item of jsThis.tableList.propertyValues) {
      list.push(item.valueName)
    }
    jsThis.tableList.intro = list.join(',')
    for (let i in jsThis.displayPropertys) {
      if (String(i) === String(jsThis.listInde)) {
        jsThis.displayPropertys.splice(i, 1, jsThis.tableList)
        jsThis.listInde = ''
        return
      }
    }
    if (!jsThis.displayPropertys) { 
      jsThis.displayPropertys = []
      jsThis.displayPropertys.push(jsThis.tableList)
    }
  }
}
