import ala from '@/service/ala'

/*
 * 添加功能 包括添加子节点和父节点
 */
export function add(array, fatherRow) {
  if (!array) {
    array = []
  }

  var data = {
    id: 0,
    name: '',
    fatherId: 0,
    status: 1,
    type: ala.urlType(),
    children: [],
    isEnable: true
  }
  if (fatherRow) {
    data.fatherId = fatherRow.id
    array.forEach(r => {
      if (r.id === fatherRow.id) {
        if (!r.children) {
          r.children = []
        }
        r.children.unshift(data)
      }
    })
  } else {
    // 父节点添加
    array.unshift(data)
  }
  return array
}
