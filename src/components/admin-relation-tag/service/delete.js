import ala from '@/service/ala'
import {
  ElMessage,
  ElMessageBox
} from 'element-plus'

// 删除
export async function del(array, row) {
  if (row.parentId > 0) {
    ala.error('存在子标签不能删除')
  }
  ElMessageBox.confirm(
      `是否删除标签:${row.name}?`,
      '提示', {
        confirmButtonText: '确定',
        cancelButtonText: '取消',
        type: 'warning',
      }
    )
    .then(async () => {
      var response = await ala.paasHttpDelete('api/Relation/Delete?id=' + row.id)
      if (response) {
        array = deleteArray(array, row)
        ala.toast('标签' + row.name + '删除成功')
        ElMessage({
          type: 'success',
          message: '标签' + row.name + '删除成功',
        })
      }
    })
    .catch(() => {
      ElMessage({
        type: 'info',
        message: '取消成功',
      })
    })
  return array
}

function deleteArray(array, row) {
  array.forEach((one, index) => {
    if (row.fatherId === 0) {
      if (one.id === row.id) {
        array.splice(index, 1)
      }
    } else {
      if (one.id === row.fatherId && one.children) {
        one.children.forEach((two, twoIndex) => {
          if (two.id === row.id) {
            one.children.splice(twoIndex, 1)
          }
        })
      }
    }
  })
}
