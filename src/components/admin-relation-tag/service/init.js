import ala from '@/service/ala'

// 初始化
export async function init() {
  var para = {
    type: ala.urlType()
  }
  var viewModel = await ala.httpGet('relationAdmin/tree')
  if (viewModel) {
    // var tag = viewModel.title.replace('标签', '')
    var tag = '商品标签'
    var title = '分类'
    viewModel.intro = `1、您可以通过${title}来灵活的管理或区分${tag},${title}支持两级分组,使用分组功能可达到更好的管理效果<br/>`
    viewModel.intro += `2、点击右上角新增按钮来新增一级${title},在表格操作一栏中可新增二级${title}和删除${title},内容修改时会自动保存.您也可以点击底部保存按钮来保存<br/>`
    viewModel.intro += `3、${title}名称不能重复。拖动可排序（有子标签的一级标签不能拖动到二级标签中），拖动保存生效`
    viewModel.title = tag
    viewModel.description = tag
  }
  if (!viewModel.data) {
    viewModel.data = []
  }
  return viewModel
}
