import ala from '@/service/ala'
import admin from '@/service/admin'

/*
 * 拖动排序 
 */
export function sort(array, newIndex, oldIndex) {
  if (!array) {
    array = []
  }
  var allArray = ala.arrayDeepAll(array)
  var currentRow = allArray.splice(oldIndex, 1)[0]
  var endRow = allArray.splice(newIndex, 1)[0]
  if (!endRow) {
    endRow = allArray.splice(newIndex - 1, 1)[0]
  }
  if (currentRow.children && currentRow.children.length > 0 && endRow.fatherId > 0) {
    ala.error('有二级标签的一级标签,不能拖动二级标签中')
    return null
  }

  // 排序
  array = ala.arrayDeepSort(array, newIndex, oldIndex)

  setTimeout(async () => {
    var response = await admin.relationSort(array)
    if (response) {
      ala.toast('保存成功')
    }
  }, 100)
  return array
}
