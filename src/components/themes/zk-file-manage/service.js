import ala from '@/service/ala'
export default {
  // 图片搜索
  async search(jsThis) {
    if (jsThis.imageListViewModel.length === 0) {
      jsThis.loading = true
      var para = {
        name: jsThis.searchModel.keyword,
        pageIndex: jsThis.searchModel.pageIndex,
        // fileType: 1,
        pageSize: jsThis.searchModel.pageSize
      }
      if (ala.filter() !== 3) {
        para.userId = ala.userId()
      }
      var response = await ala.httpGet(
        'Api/StorageFile/QueryPageList',
        para
      )
      if (response) {
        jsThis.totalCount = response.recordCount
        jsThis.imageListViewModel = response.result
      }
      jsThis.loading = false
    }
  },
  // 复制图片地址
  copyImageUrl(jsThis, url) {
    let oInput = document.createElement('input')
    oInput.style = 'position: fixed;bottom: -10000px;left: -10000px;'
    oInput.value = url
    document.body.appendChild(oInput)
    oInput.select() // 选择对象
    document.execCommand('Copy')
    jsThis.$message({
      message: '已成功复制图片地址',
      type: 'success'
    })
  },
  // 图片删除
  delete(jsThis, id) {
    jsThis
      .$confirm('此操作将永久删除该文件，是否继续？', {
        confirmButtonText: '确定',
        cancelButtonText: '取消'
      })
      .then(async () => {
        var data = {
          id
        }
        var res = await ala.httpDelete(
          'api/StorageFile/querydelete',
          data,
          jsThis.isTenant
        )
        if (res) {
          jsThis.$message({
            message: '删除成功 ',
            type: 'success'
          })
          jsThis.imageListViewModel = jsThis.imageListViewModel.filter(r => r.id !== id)
        }
      })
      .catch()
  }
}
