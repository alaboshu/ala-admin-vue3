// 所有的表单数据只从api/auto/form中获取，api/auto/save保存,统一
import ala from '@/service/ala'
export default {
  // 视图数据赋值
  getModel(autoFormConfig, dataModel) {
    var formModel = {}
    // 优先从数据库中赋值
    if (autoFormConfig && autoFormConfig.columns) {
      autoFormConfig.columns.forEach(group => {
        if ((group.value !== undefined && group.value !== null) || group.value === 0) {
          formModel[group.field] = group.value
        }
        if (group.columns) {
          group.columns.forEach(element => {
            if ((element.value !== undefined && element.value !== null) || element.value === 0 || group.value === 0) {
              formModel[element.field] = element.value
            }
          })
        }
      })
    }
    // 从URL中获取的数据
    if (dataModel) {
      if (autoFormConfig && autoFormConfig.columns) {
        autoFormConfig.columns.forEach(group => {
          if (group.columns) {
            group.columns.forEach(element => {
              var value = dataModel[element.field]
              if (!ala.strIsEmpty(value)) {
                formModel[element.field] = value
              }
            })
          } else {
            var value = dataModel[group.field]
            if (!ala.strIsEmpty(value)) {
              formModel[group.field] = value
            }
          }
        })
      }
    }
    return formModel
  }
}
